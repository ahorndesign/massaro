<div class="single-bottom-bar grid-x grid-margin-x event">
  <div class="cell small-12 medium-shrink">
    <a href="<?php echo get_post_type_archive_link('events'); ?>" class="button button-arrow success"><?php echo __('Back to resources', 'massaro');?></a>
  </div>

  <div class="cell small-12 medium-auto share-container">
    <p class="section-title"><?php _e('Share', 'massaro'); ?></p>
    <ul class="share-icons">
      <li><a class="share-icon facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"></a></li>
      <li><a class="share-icon linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>"></a></li>
      <li><a class="share-icon twitter" href="https://twitter.com/home?status=<?php the_permalink(); ?>"></a></li>
      <li><a class="share-icon email" href="mailto:?&subject=&body=<?php the_permalink(); ?>"></a></li>
      <li><button class="share-icon print" href="#" onclick="window.print();" ></button></li>	
    </ul>
  </div>
</div>