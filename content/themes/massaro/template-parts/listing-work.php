<?php
  if ( has_post_thumbnail() ) {
    $post_thumbnail_url = get_the_post_thumbnail_url();
  }
  else {
    $post_thumbnail_url = '';
  }
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('cell listing-work'); ?>>
  <a href="<?php the_permalink(); ?>">
    <h3 class="h5"><?php the_title(); ?></h3>
  <p class="work-excerpt"><?php echo wp_trim_words(get_the_excerpt(), 50, '...');  ?></p>
  <div class="listing-thumbnail" href="<?php the_permalink(); ?>" style="background-image: url('<?php echo $post_thumbnail_url; ?>')">&nbsp;</div>
  <div class="read-more">
      <?php _e('Read on', 'massaro') ?>
    </div>
  </a>
</article>