<?php
if ( is_singular('post') ) {
  $categories = get_categories( array(
    'taxonomy' => 'category',
    'exclude' => array('1')
  ));
} elseif ( is_singular('events') ) {
  $categories = get_terms( array(
    'taxonomy' => 'categories-events',
  ));
} elseif ( is_singular('webinars') ) {
  $categories = get_terms( array(
    'taxonomy' => 'categories-webinars',
  ));
} elseif ( is_singular('resources') ) {
  $categories = get_terms( array(
    'taxonomy' => 'categories-resources',
  ));
} elseif ( is_singular('case_study') ) {
  $categories = get_terms( array(
    'taxonomy' => 'categories-case-study',
  ));
}

$tags = get_the_tags();

if ( !empty($categories) || !empty($tags) ) {
  ?>
  <div class="grid-x grid-margin-x single-post-taxonomies">
    <?php if (!empty($categories)) { ?>
      <div class="cell medium-12 large-2 categories-container">
        <p class="section-title"><?php _e('Categories', 'massaro'); ?></p>
        <ul class="taxonomies-container">
          <?php foreach ($categories as $category) { ?>
            <li class="taxonomy"><a href="<?php echo get_category_link($category->term_id);?>"><?php echo $category->name; ?></a></li>
          <?php } ?>
        </ul>
      </div>
    <?php } ?>

    <?php if (!empty($tags)) { ?>
      <div class="cell medium-12 large-auto tags-container">
        <p class="section-title"><?php _e('Tags', 'massaro'); ?></p>
        <ul class="taxonomies-container">
          <?php foreach ($tags as $tag) { ?>
            <li class="taxonomy"><a href="<?php echo get_tag_link($tag);?>"><?php echo $tag->name; ?></a></li>
          <?php } ?>
        </ul>
      </div>
    <?php } ?>
  </div>
  <?php
}
?>