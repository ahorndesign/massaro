<?php
  if ( has_post_thumbnail() ) {
    $post_thumbnail_url = get_the_post_thumbnail_url(get_the_ID(), 'featured-small');
  }
  else {
    $post_thumbnail_url = '';
  }
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('cell listing-jobs'); ?>>
  <a href="<?php echo get_permalink(); ?>">
    <h4><?php the_title(); ?></h4>
  </a>
    <p class="resource-excerpt"><?php echo wp_trim_words(get_the_excerpt(), 40, '...');  ?></p>
   <div class="see-more large-button"> 
      <a href="<?php echo get_permalink(); ?>" class="wp-block-button__link has-text-color has-massaro-blue-color has-background has-secondary-background-color"><?php _e('Apply', 'massaro'); ?></a>
    </div>
</article>