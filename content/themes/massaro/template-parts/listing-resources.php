<?php
  if ( has_post_thumbnail() ) {
    $post_thumbnail_url = get_the_post_thumbnail_url(get_the_ID(), 'featured-small');
  }
  else {
    $post_thumbnail_url = '';
  }
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('cell resource-card'); ?>>
  <a href="<?php echo get_permalink(); ?>">
    <div class="resource-featured-img" style="background-image:
          linear-gradient(
            rgba(0,131,138, 0.5), 
            rgba(0,131,138, 0.5)
          ),
          url('<?php echo $post_thumbnail_url; ?>')">
      <div class="resource-date">
        <p><?php echo get_the_date( 'j' ); ?></p>
        <p><?php echo get_the_date( 'M' ); ?></p>
      </div>
    </div>

    <h6 class="resource-title"><?php the_title(); ?></h6>
    <p class="resource-excerpt"><?php echo wp_trim_words(get_the_excerpt(), 25, '...');  ?></p>
    <a href="<?php echo get_permalink(); ?>" class="button resource-read-more button-arrow success"><?php _e('Read more', 'massaro'); ?></a>
  </a>
</article>