<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>							
	<?php 
	$author_id = get_the_author_meta('ID');
	$author_url = get_author_posts_url( $author_id );
	if (get_the_author_meta('first_name')) {
		$author_name = get_the_author_meta('first_name') . ' ' . get_the_author_meta('last_name');
	}
	else {
		$author_name = get_the_author_meta('display_name');
	}
	?>
	
	<div class="default-single-top grid-container">
		<a class="go-back m-b-10" href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">
			<?php 
			if ( is_singular('post') ) {
				_e('Blog', 'massaro');
			} elseif ( get_post_type() == 'webinars') {
				_e('Webinar', 'massaro');
			} elseif ( is_singular('resources') ) {
				_e('Whitepaper', 'massaro');
			} ?>
		</a>
		
		<h1><?php the_title(); ?></h1>
		<div class="m-b-20 m-t-30 single-meta">
			<span><?php echo __('Posted', 'massaro') . ' '; ?><span class="italic"><?php echo get_the_date('F j, Y'); ?></span></span>
			<span><?php echo __('Author:', 'massaro') . ' '; ?><a class="author-name" href="<?php echo $author_url; ?>"><?php echo $author_name ?></a></span>
		</div>
	</div>

	<?php 
	if ( has_post_thumbnail() ) {
		$post_thumbnail = 'style="background-image: url(\'' . get_the_post_thumbnail_url() .'\'); background-size:cover;"';
	} else {
		$post_thumbnail = '';
	}
	?>
	
	<header class="featured-work-header" <?php echo $post_thumbnail; ?>>
		<div class="grid-container">
			<div class="featured-work-header-inner"></div>
		</div>
	</header>
	
	<div class="main-container">
		<div class="main-grid">
			<main class="main-content-full-width main-content-single">
				<?php if ( is_singular('webinars') ||  is_singular('resources') ) { ?>
					<div class="single-resources-top-bar grid-x grid-margin-x">
						<?php
						$read_time = get_field('resources_read_time');
						?>

						<?php if ( $read_time ) { ?>
							<div class="cell auto read-time">
								<span><?php echo $read_time . ' ' . __('read', 'massaro'); ?></span>
							</div>
						<?php } ?>

						<div class="cell auto social-share">
							<ul class="share-icons">
								<li><a class="share-icon linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>"></a></li>
								<li><a class="share-icon twitter" href="https://twitter.com/home?status=<?php the_permalink(); ?>"></a></li>	
							</ul>
						</div>
					</div>
				<?php } ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-content post-content">
						<?php the_content(); ?>
						<?php edit_post_link( __( '(Edit)', 'foundationpress' ), '<span class="edit-link">', '</span>' ); ?>
					</div>
				</article>

				<?php if ( is_singular('webinars') ||  is_singular('resources') ) { ?>
					<?php get_template_part( 'template-parts/single-bottom-bar'); ?>
				<?php } else {
					$users_job_position = get_field('users_job_position', 'user_'.$author_id);
					$users_image = get_field('users_image', 'user_'.$author_id); ?>

					<div class="post-author-info-container grid-x grid-margin-x">
						<?php if ( $users_image ) { ?>
							<div class="cell small-12 medium-2">
								<img class="author-image" src="<?php echo $users_image['sizes']['medium']; ?>" alt="Author Image">
							</div>
						<?php } ?>
						<div class="cell small-12 medium-auto author-info">
							<p class="section-title"><?php _e('Written by', 'massaro'); ?></p>
							<h4 class="author-name"><?php echo $author_name; ?></h4>
							<?php if ( $users_job_position ) { ?>
								<p class="job-position"><?php echo $users_job_position; ?></p>
							<?php } ?>

							<a href="<?php echo $author_url; ?>" class="button button-arrow success"><?php echo __('See', 'massaro') .' '. $author_name . '\'s ' . __('bio', 'massaro'); ?></a>
						</div>
						<div class="cell small-12 medium-shrink share-container">
							<p class="section-title"><?php _e('Share this post', 'massaro'); ?></p>
							<ul class="share-icons">
								<li><a class="share-icon facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"></a></li>
								<li><a class="share-icon linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>"></a></li>
								<li><a class="share-icon twitter" href="https://twitter.com/home?status=<?php the_permalink(); ?>"></a></li>
								<li><a class="share-icon email" href="mailto:?&subject=&body=<?php the_permalink(); ?>"></a></li>
								<li><button class="share-icon print" href="#" onclick="window.print();" ></button></li>	
							</ul>
						</div>
					</div>
				<?php } ?>

				<?php get_template_part( 'template-parts/single-taxonomies-bar'); ?>

			</main>
		</div>
	</div>
<?php endwhile; ?>
<?php get_footer();