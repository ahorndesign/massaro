import $ from 'jquery';
import whatInput from 'what-input';
import ScrollOut from "scroll-out";

window.$ = $;

import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';


$(document).foundation();

// Scroll-out install (that simple)
ScrollOut({
    threshold: 0.99
});


import 'owl.carousel';

$('.owl-quotes').owlCarousel({
    autoplay: false,
    center: true,
    loop: true,
    dots: false,
    nav: true,
    margin: 30,
    navText: ["<div class='left-nav'></div>", "<div class='right-nav'></div>"],
    responsive: {
        // breakpoint from 0 up
        0: {
            items: 1,
        },
        // breakpoint from 480 up
        480: {
            items: 1,
        },
        // breakpoint from 768 up
        768: {
            items: 3,
        }
    }
});

$('.fable-slider').owlCarousel({
    autoplay: false,
    center: true,
    loop: false,
    dots: true,
    margin: 30,
    items: 1,
    touchDrag: false,
    mouseDrag: false,
    smartSpeed: 500,
    autoHeight: true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    responsive: {
        480: {
            autoHeight: false,
        },
    }

});

$(function () {
    $('.am-next').click(function () {
        $('.fable-slider').trigger('next.owl.carousel');
    });
    $('.am-prev').click(function () {
        $('.fable-slider').trigger('prev.owl.carousel');
    });
    $('.owl-nav').appendTo($('.g-quotes-outer').find('h2'));
});


$(document).ready(() => {
    if ($(window).width() < 500) {
        $("#mobile-nav").css('display', 'none');
    }
});


$('button.menu-icon-nav').on('click', () => {
    $("#mobile-nav").toggle();
});


$(function () {
    function init() {
        // Basic options for a simple Google Map
        // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
        var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: 13,
            // The latitude and longitude to center the map (always required)
            center: new google.maps.LatLng(42.446, -71.324), // New York
            // How you would like to style the map.
            // This is where you would paste any style found on Snazzy Maps.
            styles: [
                {
                    "featureType": "all",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "all",
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#3e606f"
                        },
                        {
                            "weight": 2
                        },
                        {
                            "gamma": 0.84
                        }
                    ]
                },
                {
                    "featureType": "all",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "weight": 0.6
                        },
                        {
                            "color": "#1a3541"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#2c5a71"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#406d80"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#2c5a71"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#29768a"
                        },
                        {
                            "lightness": -37
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#406d80"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#193341"
                        }
                    ]
                }
            ]
        }
        var mapElement = document.getElementById('map');
        var map = new google.maps.Map(mapElement, mapOptions);
        function pinSymbol(color) {
            return {
                path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z M -2,-30 a 2,2 0 1,1 4,0 2,2 0 1,1 -4,0',
                fillColor: color,
                fillOpacity: 1,
                strokeColor: '#000',
                strokeWeight: 2,
                scale: 1,
            };
        };

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(42.4402982, -71.3326206),
            icon: pinSymbol("#D1E317"),
            map: map,
        });
    };

    google.maps.event.addDomListener(window, 'load', init);

});


$(function () {
    // home anchor scroll to next section
    $('.home-scroll-down a').click(function (e) {
        var navHeight = $('.site-navigation').height();
        e.preventDefault();
        $('html,body').animate({ scrollTop: $('#promise').offset().top - navHeight - 150 }, 'slow');
    });
});

$("#team-left").click(function () {
    var p = $(".massaro-experts-inner").first();
    $(p).scrollLeft(p.scrollLeft() - 300);
});

$("#team-right").click(function () {
    var p = $(".massaro-experts-inner").first();
    $(p).scrollLeft(p.scrollLeft() + 300);
});

$(function () {
    if ($(window).width() < 481) {
        $('#menu-mobile-menu .is-accordion-submenu-parent').click(function () {
            $(this).toggleClass('open');
        });
    }
});