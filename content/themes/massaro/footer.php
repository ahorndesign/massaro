<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

	// $footer_copy = get_field('footer_copy', 'option');	
	$footer_copy = '© 2019 - ' . date("Y") . ' Massaro. All rights reserved.';
	
	$footer_location = get_field('footer_location', 'option');
	// socials
	$footer_linkedin = get_field('footer_linkedin', 'option');
	$footer_twitter = get_field('footer_twitter', 'option');
	$footer_facebook = get_field('footer_facebook', 'option');



?>

<footer class="site-footer">
	<div class="footer-container">
		<div class="footer-grid grid-x">
			<div class="footer-left cell small-12 medium-auto">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
					<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/assets/images/footer_logo_massaro_white.svg" alt="logo" class="print-display-none"/>
					<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/assets/images/massaro_logo.svg" alt="logo" class="print-display-block"/>
				</a>
				<p class="notice">
					<small>
						<?php echo $footer_copy ?>
					</small>
				</p>
			</div>
			<div class="footer-socials small-6 medium-shrink"> 
				<?php
					if($footer_linkedin !== '') { ?>
						<a class="link-linkedin" href="<?php echo $footer_linkedin ?>"><span></span></a>
					<?php }
					if($footer_twitter !== '') { ?>
						<a class="link-twitter" href="<?php echo $footer_twitter ?>"><span></span></a>
					<?php }
					if($footer_facebook !== '') { ?>
						<a class="link-facebook" href="<?php echo $footer_facebook ?>"><span></span></a>
					<?php }
				
				?>
			</div>
			<div class="footer-location small-6 medium-shrink"> 
			<?php echo $footer_location ?>
			</div>
			<div class="footer-menu-left small-6 medium-shrink"> 
				<?php footer_nav_left(); ?>
			</div>
			<div class="footer-menu-right small-6 medium-shrink"> 
				<?php footer_nav_right(); ?>
			</div>
		</div>
	</div>
</footer>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
	</div><!-- Close off-canvas content -->
<?php endif; ?>

<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAE9Ah7kiemBFnCZFmUTWWBsVWMJDxjXPc'></script>
<?php wp_footer(); ?>
</body>
</html>
