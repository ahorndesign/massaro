<?php
/*
 * Template Name: Featured Work
 * Template Post Type: post, our-work
 */

 $company_size = get_field('featured_work_company_size', get_the_ID());
 $industry = get_field('featured_work_industry', get_the_ID());

get_header(); ?>

<header class="featured-work-header">
	<div class="grid-container">
		<div class="grid-x grid-margin-x featured-work-header-inner">
			<div class="cell small-12 medium-auto">
				<h1><?php the_title(); ?></h1>
				<div class="featured-work-meta">
					<?php if ($industry) { ?>
						<span>
							<?php _e('Industry:', 'massaro') ?> 
							<span class="green-meta"><?php echo $industry; ?></span>
						</span>
					<?php } ?>

					<?php if ($company_size) { ?>
						<span>
							<?php _e('Company Size:', 'massaro') ?>
							<span class="green-meta"><?php echo $company_size; ?></span>
						</span>
					<?php } ?>
				</div>
			</div>

			<?php if ( has_post_thumbnail() ) { ?>
				<div class="cell small-12 medium-6 featured-work-header-logo">
					<img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" />
				</div>
			<?php } ?>
		</div>
	</div>
</header>

<div class="main-container">
	<div class="main-grid">
		<main class="main-content-single">
			<?php while ( have_posts() ) : the_post(); ?>							
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-content">
						<?php the_content(); ?>
						<?php edit_post_link( __( '(Edit)', 'foundationpress' ), '<span class="edit-link">', '</span>' ); ?>
					</div>
				</article>
			<?php endwhile; ?>
			
			<div class="single-bottom-bar grid-x grid-margin-x featured-work">
				<div class="cell small-12 medium-auto share-container">
					<p class="section-title"><?php _e('Share', 'massaro'); ?></p>
					<ul class="share-icons">
						<li><a class="share-icon facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"></a></li>
						<li><a class="share-icon linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>"></a></li>
						<li><a class="share-icon twitter" href="https://twitter.com/home?status=<?php the_permalink(); ?>"></a></li>
						<li><a class="share-icon email" href="mailto:?&subject=&body=<?php the_permalink(); ?>"></a></li>
						<li><button class="share-icon print" href="#" onclick="window.print();" ></button></li>	
					</ul>
				</div>
				
				<div class="cell small-12 medium-shrink next-post-link">
					<?php echo next_post_link( '%link', __('Read the next case study', 'massaro') ); ?>
				</div>
			</div>

			<?php get_template_part( 'template-parts/single-taxonomies-bar'); ?>

		</main>
	</div>
</div>
<?php get_footer();