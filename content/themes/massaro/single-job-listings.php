<?php
/*
 * Template Name: Featured Work
 * Template Post Type: post, our-work
 */

 $company_size = get_field('featured_work_company_size', get_the_ID());
 $industry = get_field('featured_work_industry', get_the_ID());

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>							
	

	<?php 
	if ( has_post_thumbnail() ) {
		$post_thumbnail = 'style="background-image: url(\'' . get_the_post_thumbnail_url() .'\');"';
	} else {
		$post_thumbnail = '';
	}
	?>
	
	<header class="job-listing-header" <?php echo $post_thumbnail; ?>>
		<div class="grid-container">
			<div class="job-listing-header-inner"></div>
		</div>
	</header>
	
	<div class="main-wrapper-job-listings">
        <main class="main-content-full-width main-content-single-job-listings ">
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="entry-content post-content">
                    <?php the_content(); ?>
                </div>
            </article>

        </main>
	</div>
<?php endwhile; ?>

<?php get_footer();