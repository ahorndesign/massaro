	<?php
/*
 * Template Name: Event
 * Template Post Type: events
 */

get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>	
	<?php 
	if (get_the_author_meta('first_name')) {
		$author_name = get_the_author_meta('first_name') . ' ' . get_the_author_meta('last_name');
	} else {
		$author_name = get_the_author_meta('display_name');
	}
	?>
	
	<div class="default-single-top grid-container">
		<a class="go-back m-b-10" href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">
			<?php _e('Event', 'massaro'); ?>
		</a>
		<h1><?php the_title(); ?></h1>
		<div class="m-b-20 m-t-30 single-meta">
			<span><?php echo __('Posted', 'massaro') . ' '; ?><span class="italic"><?php echo get_the_date('F j, Y'); ?></span></span>
			<span><?php echo __('Author:', 'massaro') . ' '; ?><a class="author-name" href="<?php echo get_author_posts_url( $post->post_author ); ?>"><?php echo $author_name ?></a></span>
		</div>
	</div>

	<?php 
	if ( has_post_thumbnail() ) {
		$post_thumbnail = 'style="background-image: url(\'' . get_the_post_thumbnail_url() .'\'); background-size:cover;"';
	} else {
		$post_thumbnail = '';
	}
	?>

	<header class="featured-work-header event" <?php echo $post_thumbnail ?>>
		<div class="grid-container">
			<div class="featured-work-header-inner">		
			</div>
		</div>
	</header>
	
	<div class="main-container">
		<div class="main-grid">
			<main class="main-content-full-width main-content-single event">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-content">
						<div class="grid-x">

							
							<div class="cell small-12 large-offset-1 large-3 large-order-2">
								<div class="event-box">
									<?php
									$event_start = get_field('event_start_date');
									$event_end = get_field('event_end_date');
									$event_location = get_field('event_location'); 

	
									$event_start = DateTime::createFromFormat('YFj', $event_start);
									$event_end = DateTime::createFromFormat('YFj', $event_end);
									?>

									<p class="month"><?php echo $event_start->format('F'); ?></p>
									<p class="days"><?php echo $event_start->format('j') . '-' . $event_end->format('j')?></p>

									<p class="location"><?php echo $event_location; ?></p>
								</div>
							</div>

							<div class="cell small-12 large-8 post-content large-order-1 event-content">
								<?php the_content(); ?>
							</div>


						</div>

						<?php 
						$event_details_url = get_field('event_details_url');
						if ($event_details_url) { ?>
							<a href="<?php echo $event_details_url; ?>" class="event-details button button-arrow success"><?php echo __('View event details', 'massaro');?></a>
						<?php } ?>

						<?php edit_post_link( __( '(Edit)', 'foundationpress' ), '<span class="edit-link">', '</span>' ); ?>
					</div>
				</article>

				<div class="single-bottom-bar grid-x grid-margin-x event">
					<div class="cell small-12 medium-shrink">
						<a href="<?php echo get_post_type_archive_link('events'); ?>" class="button button-arrow success"><?php echo __('Back to resources', 'massaro');?></a>
					</div>

					<div class="cell small-12 medium-auto share-container">
						<p class="section-title"><?php _e('Share', 'massaro'); ?></p>
						<ul class="share-icons">
							<li><a class="share-icon facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"></a></li>
							<li><a class="share-icon linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>"></a></li>
							<li><a class="share-icon twitter" href="https://twitter.com/home?status=<?php the_permalink(); ?>"></a></li>
							<li><a class="share-icon email" href="mailto:?&subject=&body=<?php the_permalink(); ?>"></a></li>
							<li><button class="share-icon print" href="#" onclick="window.print();" ></button></li>	
						</ul>
					</div>
				</div>
			
				<?php get_template_part( 'template-parts/single-taxonomies-bar'); ?>
				
			</main>
		</div>
	</div>
<?php endwhile; ?>

<?php get_footer();