<?php

if ( ! function_exists( 'foundationpress_gutenberg_support' ) ) :
	function foundationpress_gutenberg_support() {

    // Add foundation color palette to the editor
    add_theme_support( 'editor-color-palette', array(
        array(
            'name' => __( 'Primary Color', 'foundationpress' ),
            'slug' => 'primary',
            'color' => '#00838A',
        ),
        array(
            'name' => __( 'Secondary Color', 'foundationpress' ),
            'slug' => 'secondary',
            'color' => '#D1E317',
        ),
        array(
            'name' => __( 'Success Color', 'foundationpress' ),
            'slug' => 'success',
            'color' => '#3FC9E6',
        ),
        array(
            'name' => __( 'Warning color', 'foundationpress' ),
            'slug' => 'warning',
            'color' => '#FFB511',
        ),
        array(
            'name' => __( 'Alert color', 'foundationpress' ),
            'slug' => 'alert',
            'color' => '#cc4b37',
        ),
        array(
            'name' => __( 'Massaro color', 'foundationpress' ),
            'slug' => 'massaro-blue',
            'color' => '#0A2133',
        ),
        array(
            'name' => __( 'White color', 'foundationpress' ),
            'slug' => 'white',
            'color' => '#fefefe',
        ),
        array(
            'name' => __( 'Lime color', 'foundationpress' ),
            'slug' => 'lime',
            'color' => '#C0C930',
        )
    ) );

	}

	add_action( 'after_setup_theme', 'foundationpress_gutenberg_support' );
endif;
