<?php
/**
 * Register widget areas
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

if ( ! function_exists( 'foundationpress_sidebar_widgets' ) ) :
	function foundationpress_sidebar_widgets() {
		register_sidebar(
			array(
				'id'            => 'sidebar-widgets',
				'name'          => __( 'Sidebar widgets', 'foundationpress' ),
				'description'   => __( 'Drag widgets to this sidebar container.', 'foundationpress' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h6>',
				'after_title'   => '</h6>',
			)
		);

		register_sidebar(
			array(
				'id'            => 'footer-widgets',
				'name'          => __( 'Footer widgets', 'foundationpress' ),
				'description'   => __( 'Drag widgets to this footer container', 'foundationpress' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h6>',
				'after_title'   => '</h6>',
			)
		);

		register_sidebar(
			array(
				'id'            => 'archive-footer-top-header-widgets',
				'name'          => __( 'Archive Footer Top Header Widgets', 'foundationpress' ),
				'description'   => __( 'Drag widgets to this container', 'foundationpress' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s has-text-align-center m-b-50">',
				'after_widget'  => '</section>',
				'before_title'  => '<h3 class="has-text-align-center h4 m-b-20">',
				'after_title'   => '</h3>',
			)
		);

		register_sidebar(
			array(
				'id'            => 'archive-footer-top-widgets',
				'name'          => __( 'Archive Footer Top Widgets', 'foundationpress' ),
				'description'   => __( 'Drag widgets to this container', 'foundationpress' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s cell small-12 medium-4 archive-footer-top-cell">',
				'after_widget'  => '</section>',
				'before_title'  => '<div class="top-number"><h4>',
				'after_title'   => '</h4></div>',
			)
		);
	}

	add_action( 'widgets_init', 'foundationpress_sidebar_widgets' );
endif;
