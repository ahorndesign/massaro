<?php 
add_action( 'rest_api_init', function () {
	register_rest_route( 'hlwp/v1', '/terms/(?P<id>\d+)', array(
		'methods' => 'GET',
		'callback' => 'get_taxonomy_rest',
		'permission_callback' =>  '__return_true'
	) );
} );
function get_taxonomy_rest( $data ) {
	global $post;
	global $wp_query;
	$response = array();
	$terms = get_terms( array(
	'taxonomy' => array('category'),
    'hide_empty' => false,
	) );

	if ( empty( $terms ) ) {
		return null;
	}
	foreach ($terms as $key => $term) {
		$response[ $term->term_id ] = $term->name;
	}
	return rest_ensure_response( $response );
}


