<?php
// Register Event Taxonomy
function event_tax() {

	$labels = array(
		'name'                       => _x( 'Categories', 'Taxonomy General Name', 'massaro' ),
		'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'massaro' ),
		'menu_name'                  => __( 'Categories', 'massaro' ),
		'all_items'                  => __( 'All Categories', 'massaro' ),
		'parent_item'                => __( 'Parent Category', 'massaro' ),
		'parent_item_colon'          => __( 'Parent Category:', 'massaro' ),
		'new_item_name'              => __( 'New Category Name', 'massaro' ),
		'add_new_item'               => __( 'Add New Category', 'massaro' ),
		'edit_item'                  => __( 'Edit Category', 'massaro' ),
		'update_item'                => __( 'Update Category', 'massaro' ),
		'view_item'                  => __( 'View Category', 'massaro' ),
		'separate_items_with_commas' => __( 'Separate categories with commas', 'massaro' ),
		'add_or_remove_items'        => __( 'Add or remove categories', 'massaro' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'massaro' ),
		'popular_items'              => __( 'Popular Categories', 'massaro' ),
		'search_items'               => __( 'Search Categories', 'massaro' ),
		'not_found'                  => __( 'Not Found', 'massaro' ),
		'no_terms'                   => __( 'No categories', 'massaro' ),
		'items_list'                 => __( 'Categories list', 'massaro' ),
		'items_list_navigation'      => __( 'Categories list navigation', 'massaro' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'categories-events', array( 'events' ), $args );

}
add_action( 'init', 'event_tax', 0 );


// Register Webinar Taxonomy
function webinar_tax() {

	$labels = array(
		'name'                       => _x( 'Categories', 'Taxonomy General Name', 'massaro' ),
		'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'massaro' ),
		'menu_name'                  => __( 'Categories', 'massaro' ),
		'all_items'                  => __( 'All Categories', 'massaro' ),
		'parent_item'                => __( 'Parent Category', 'massaro' ),
		'parent_item_colon'          => __( 'Parent Category:', 'massaro' ),
		'new_item_name'              => __( 'New Category Name', 'massaro' ),
		'add_new_item'               => __( 'Add New Category', 'massaro' ),
		'edit_item'                  => __( 'Edit Category', 'massaro' ),
		'update_item'                => __( 'Update Category', 'massaro' ),
		'view_item'                  => __( 'View Category', 'massaro' ),
		'separate_items_with_commas' => __( 'Separate categories with commas', 'massaro' ),
		'add_or_remove_items'        => __( 'Add or remove categories', 'massaro' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'massaro' ),
		'popular_items'              => __( 'Popular Categories', 'massaro' ),
		'search_items'               => __( 'Search Categories', 'massaro' ),
		'not_found'                  => __( 'Not Found', 'massaro' ),
		'no_terms'                   => __( 'No categories', 'massaro' ),
		'items_list'                 => __( 'Categories list', 'massaro' ),
		'items_list_navigation'      => __( 'Categories list navigation', 'massaro' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'categories-webinars', array( 'webinars' ), $args );

}
add_action( 'init', 'webinar_tax', 0 );


// Register Resource Taxonomy
function resource_tax() {

	$labels = array(
		'name'                       => _x( 'Categories', 'Taxonomy General Name', 'massaro' ),
		'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'massaro' ),
		'menu_name'                  => __( 'Categories', 'massaro' ),
		'all_items'                  => __( 'All Categories', 'massaro' ),
		'parent_item'                => __( 'Parent Category', 'massaro' ),
		'parent_item_colon'          => __( 'Parent Category:', 'massaro' ),
		'new_item_name'              => __( 'New Category Name', 'massaro' ),
		'add_new_item'               => __( 'Add New Category', 'massaro' ),
		'edit_item'                  => __( 'Edit Category', 'massaro' ),
		'update_item'                => __( 'Update Category', 'massaro' ),
		'view_item'                  => __( 'View Category', 'massaro' ),
		'separate_items_with_commas' => __( 'Separate categories with commas', 'massaro' ),
		'add_or_remove_items'        => __( 'Add or remove categories', 'massaro' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'massaro' ),
		'popular_items'              => __( 'Popular Categories', 'massaro' ),
		'search_items'               => __( 'Search Categories', 'massaro' ),
		'not_found'                  => __( 'Not Found', 'massaro' ),
		'no_terms'                   => __( 'No categories', 'massaro' ),
		'items_list'                 => __( 'Categories list', 'massaro' ),
		'items_list_navigation'      => __( 'Categories list navigation', 'massaro' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'categories-resources', array( 'resources' ), $args );

}
add_action( 'init', 'resource_tax', 0 );


// Register Case Studies Taxonomy
function case_studies_tax() {

	$labels = array(
		'name'                       => _x( 'Categories', 'Taxonomy General Name', 'massaro' ),
		'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'massaro' ),
		'menu_name'                  => __( 'Categories', 'massaro' ),
		'all_items'                  => __( 'All Categories', 'massaro' ),
		'parent_item'                => __( 'Parent Category', 'massaro' ),
		'parent_item_colon'          => __( 'Parent Category:', 'massaro' ),
		'new_item_name'              => __( 'New Category Name', 'massaro' ),
		'add_new_item'               => __( 'Add New Category', 'massaro' ),
		'edit_item'                  => __( 'Edit Category', 'massaro' ),
		'update_item'                => __( 'Update Category', 'massaro' ),
		'view_item'                  => __( 'View Category', 'massaro' ),
		'separate_items_with_commas' => __( 'Separate categories with commas', 'massaro' ),
		'add_or_remove_items'        => __( 'Add or remove categories', 'massaro' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'massaro' ),
		'popular_items'              => __( 'Popular Categories', 'massaro' ),
		'search_items'               => __( 'Search Categories', 'massaro' ),
		'not_found'                  => __( 'Not Found', 'massaro' ),
		'no_terms'                   => __( 'No categories', 'massaro' ),
		'items_list'                 => __( 'Categories list', 'massaro' ),
		'items_list_navigation'      => __( 'Categories list navigation', 'massaro' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'categories-case-study', array( 'case_study' ), $args );

}
add_action( 'init', 'case_studies_tax', 0 );