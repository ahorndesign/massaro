<?php
function hswp_dynamic_block_func( $attributes, $content ) {
        global $post;
        if( empty($attributes['posts_per_page'] ) ){
            $attributes['posts_per_page']  = 1;
        }

        if( empty($attributes['template'] ) ){
            $attributes['template']  = 'content.php';
        }

        if( empty($attributes['post_type'] ) ){
            $attributes['post_type']  = 'post';
        }
         if( empty($attributes['post_type'] ) ){
            $taxonomies = get_object_taxonomies( $attributes['post_type'] );
            $tax = '';
            foreach ($taxonomies as $key => $taxonomy) {
                $term = get_term( $attributes['category'], $taxonomy );
                if( !empty($term)){
                    $tax = $term->taxonomy;
                }
            }
        }
    
        $args = array(
            'post_type' => $attributes['post_type'],
            'posts_per_page' => $attributes['posts_per_page'],
        );

        if ($attributes['post_type'] === 'resources' ) {
            $args['tax_query'] = array(
                                    array(
                                        'taxonomy' => 'resource-type',
                                        'field'    => 'term_id',
                                        'terms'    => (int)$attributes['category'],
                                    ) 
                                );
                            }
        
        $result = '';  
        $path = get_template_directory() . '/template-parts/';
        ob_start();
        $the_query = new WP_Query( $args );
        // The Loop
        if($attributes['template'] === 'content-blog.php' ) {
            $additional_class = "blog-grid-alt";
        }
        if($attributes['template'] === 'listing-deadlines.php' ) {
            $additional_class = "listing-deadlines-grid";
        }
        if($attributes['template'] === 'listing-jobs.php' ) {
            $additional_class = "listing-jobs-grid grid-margin-y m-b-40";
        } 
        // add the grid-x classes if its not this tempalte
        if($attributes['template'] != 'upcoming-events-three.php' ) {
            $main_classes = "grid-x grid-margin-y grid-margin-x";
        }
        ?>
        
        <div class="grid-container">
        <div class="<?php echo !empty($main_classes) ? $main_classes : false ?> small-up-<?php echo $attributes['small'];?> medium-up-<?php echo $attributes['medium'];?> large-up-<?php echo $attributes['large'];?> <?php echo !empty($additional_class) ? $additional_class : false  ?>   " >
            <?php
            if ( $the_query->have_posts() ) {
                while ( $the_query->have_posts() ) {
                    $the_query->the_post();
                    ?>
                    <?php         
                    // remove the cell el if the template this since its not a conventional post grid
                        if($attributes['template'] != 'upcoming-events-three.php' ) {
                    ?>
					<div class="cell">
                    <?php } ?>
						<?php
						require($path.$attributes['template']);
                        ?>
                    <?php if($attributes['template'] != 'upcoming-events-three.php' ) { ?>    
					</div> <?php } ?>
					<?php
                }
                /* Restore original Post Data */
                wp_reset_postdata();
            } else {
                echo 'no posts found';
            }
            ?>
        </div>  
        </div>
        <?php
        $result = ob_get_contents();
        ob_end_clean();
    return $result;
}

register_block_type( 'edx/post-grid', array(
    'attributes'      => array(
            'post_type'    => array(
                'type'      => 'string',
                'default'   => 'post',
            ),
            'posts_per_page' => array(
                'type'      => 'number',
                'default'   => 1,
            ),
            'category' => array(
                'type'      => 'string',
                'default'   => '',
            ),
            'template' => array(
                'type'      => 'string',
                'default'   => '',
			),
			'small' => array(
                'type'      => 'number',
                'default'   => 1,
			),
			'medium' => array(
                'type'      => 'number',
                'default'   => 1,
			),
			'large' => array(
                'type'      => 'number',
                'default'   => 1,
            ),
        ),
    'render_callback' => 'hswp_dynamic_block_func',
) );