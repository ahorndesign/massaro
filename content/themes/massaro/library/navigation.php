<?php
/**
 * Register Menus
 *
 * @link http://codex.wordpress.org/Function_Reference/register_nav_menus#Examples
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

register_nav_menus(
	array(
		'top-bar-r'  => esc_html__( 'Right Top Bar', 'foundationpress' ),
		'mobile-nav' => esc_html__( 'Mobile', 'foundationpress' ),
		'footer_nav_left' => esc_html__( 'Footer nav left', 'foundationpress' ),
		'footer_nav_right' => esc_html__( 'Footer nav right', 'foundationpress' ),
		'resources-nav' => esc_html__( 'Resources nav', 'foundationpress' ),
		'blog-filter' => esc_html__( 'Blog filter', 'foundationpress' ),
		'events-filter' => esc_html__( 'Events filter', 'foundationpress' ),
		'webinars-filter' => esc_html__( 'Webinars filter', 'foundationpress' ),
		'whitepapers-filter' => esc_html__( 'White Papers filter', 'foundationpress' ),
	)
);


/**
 * Desktop navigation - right top bar
 *
 * @link http://codex.wordpress.org/Function_Reference/wp_nav_menu
 */
if ( ! function_exists( 'foundationpress_top_bar_r' ) ) {
	function foundationpress_top_bar_r() {
		wp_nav_menu(
			array(
				'container'      => false,
				'menu_class'     => 'dropdown menu desktop-menu',
				'items_wrap'     => '<ul id="%1$s" class="%2$s" data-dropdown-menu>%3$s</ul>',
				'theme_location' => 'top-bar-r',
				'depth'          => 3,
				'fallback_cb'    => false,
				'walker'         => new Foundationpress_Top_Bar_Walker(),
			)
		);
	}
}

/**
 * Footer nav col 1
 *
 * @link http://codex.wordpress.org/Function_Reference/wp_nav_menu
 */
if ( ! function_exists( 'footer_nav_left' ) ) {
	function footer_nav_left() {
		wp_nav_menu(
			array(
				'container'      => false,
				'menu_class'     => 'dropdown menu vertical',
				'items_wrap'     => '<ul id="%1$s" class="%2$s" data-dropdown-menu>%3$s</ul>',
				'theme_location' => 'footer_nav_left',
				'depth'          => 3,
				'fallback_cb'    => false,
				'walker'         => new Foundationpress_Top_Bar_Walker(),
			)
		);
	}
}
/**
 * Footer nav col 2
 *
 * @link http://codex.wordpress.org/Function_Reference/wp_nav_menu
 */
if ( ! function_exists( 'footer_nav_right' ) ) {
	function footer_nav_right() {
		wp_nav_menu(
			array(
				'container'      => false,
				'menu_class'     => 'dropdown menu',
				'items_wrap'     => '<ul id="%1$s" class="%2$s" data-dropdown-menu>%3$s</ul>',
				'theme_location' => 'footer_nav_right',
				'depth'          => 3,
				'fallback_cb'    => false,
				'walker'         => new Foundationpress_Top_Bar_Walker(),
			)
		);
	}
}

/**
 * Mobile navigation - topbar (default) or offcanvas
 */
if ( ! function_exists( 'foundationpress_mobile_nav' ) ) {
	function foundationpress_mobile_nav() {
		wp_nav_menu(
			array(
				'container'      => false,                         // Remove nav container
				'menu'           => __( 'mobile-nav', 'foundationpress' ),
				'menu_class'     => 'vertical menu',
				'theme_location' => 'mobile-nav',
				'items_wrap'     => '<ul id="%1$s" class="%2$s" data-accordion-menu data-submenu-toggle="true">%3$s</ul>',
				'fallback_cb'    => false,
				'walker'         => new Foundationpress_Mobile_Walker(),
			)
		);
	}
}

if ( ! function_exists( 'resources_nav' ) ) {
	function resources_nav() {
		wp_nav_menu(
			array(
				'container'      => false,
				'menu_class'     => 'menu',
				'items_wrap'     => '<ul id="%1$s" class="%2$s" >%3$s</ul>',
				'theme_location' => 'resources-nav',
				'depth'          => 1,
				'fallback_cb'    => false,
			)
		);
	}
}

if ( ! function_exists( 'resources_filter' ) ) {
	function resources_filter() {
		$nav_args = array();
		if ( is_post_type_archive('events') ) {
			$nav_args = array(
				'container'      => false,
				'menu_class'     => ' menu vertical resources-filter',
				'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'theme_location' => 'events-filter',
				'depth'          => 2,
				'fallback_cb'    => false,
				'echo' => false
			);
		} elseif ( is_post_type_archive('webinars') ) {
			$nav_args = array(
				'container'      => false,
				'menu_class'     => ' menu vertical resources-filter',
				'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'theme_location' => 'webinars-filter',
				'depth'          => 2,
				'fallback_cb'    => false,
				'echo' => false
			);
		} elseif ( is_post_type_archive('resources') ) {
			$nav_args = array(
				'container'      => false,
				'menu_class'     => ' menu vertical resources-filter',
				'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'theme_location' => 'whitepapers-filter',
				'depth'          => 2,
				'fallback_cb'    => false,
				'echo' => false
			);
		} elseif ( is_home() || is_category() || is_tag() ) {
			$nav_args = array(
				'container'      => false,
				'menu_class'     => ' menu vertical resources-filter',
				'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'theme_location' => 'blog-filter',
				'depth'          => 2,
				'fallback_cb'    => false,
				'echo' => false
			);
		} else {
			return false;
		}

		if ( wp_nav_menu($nav_args) ) {
			return $nav_args;
		}
	}
}



/**
 * Add support for buttons in the top-bar menu:
 * 1) In WordPress admin, go to Apperance -> Menus.
 * 2) Click 'Screen Options' from the top panel and enable 'CSS CLasses' and 'Link Relationship (XFN)'
 * 3) On your menu item, type 'has-form' in the CSS-classes field. Type 'button' in the XFN field
 * 4) Save Menu. Your menu item will now appear as a button in your top-menu
*/
if ( ! function_exists( 'foundationpress_add_menuclass' ) ) {
	function foundationpress_add_menuclass( $ulclass ) {
		$find    = array( '/<a rel="button"/', '/<a title=".*?" rel="button"/' );
		$replace = array( '<a rel="button" class="button"', '<a rel="button" class="button"' );

		return preg_replace( $find, $replace, $ulclass, 1 );
	}
	add_filter( 'wp_nav_menu', 'foundationpress_add_menuclass' );
}
