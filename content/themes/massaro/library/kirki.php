<?php
Kirki::add_config( 'massaro', array(
	'capability' => 'edit_theme_options',
	'option_type' => 'theme_mod',
	'option_name' => 'massaro'
) );

// Add section
Kirki::add_section( 'archives_section', array(
	'title'          => __('Archives section', 'massaro'),
	'priority'       => 11
) );


// For the resources
Kirki::add_field( 'massaro', array(
	'type'     => 'generic',
	'settings' => 'archive_resources_title',
	'label'    => __( 'Resources archive title', 'massaro' ),
	'section'  => 'archives_section',
	'default'  => esc_attr__( 'Resources', 'massaro' ),
	'tooltip'  => 'Please the title for the resources archive',
	'priority' => 1,
) );
Kirki::add_field( 'massaro', array(
	'type'     => 'textarea',
	'settings' => 'archive_resources_desc',
	'label'    => __( 'Resources archive description', 'massaro' ),
	'section'  => 'archives_section',
	'default'  => esc_attr__( '', 'massaro' ),
	'tooltip'  => 'Please the description for the resources archive',
	'priority' => 2,
) );
Kirki::add_field( 'massaro', array(
	'type'     => 'image',
	'settings' => 'header_resources_image',
	'label'    => __( 'Resources Archive Image', 'massaro' ),
	'section'  => 'archives_section',
	'tooltip'  => 'Set the header image for resources archive',
	'priority' => 3,
) );