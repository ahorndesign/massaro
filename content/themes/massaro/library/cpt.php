<?php
// Register Events Post Type
function events() {

	$labels = array(
		'name'                  => _x( 'Events', 'Post Type General Name', 'massaro' ),
		'singular_name'         => _x( 'Event', 'Post Type Singular Name', 'massaro' ),
		'menu_name'             => __( 'Events', 'massaro' ),
		'name_admin_bar'        => __( 'Event', 'massaro' ),
		'archives'              => __( 'Event Archives', 'massaro' ),
		'attributes'            => __( 'Event Attributes', 'massaro' ),
		'parent_item_colon'     => __( 'Parent Event:', 'massaro' ),
		'all_items'             => __( 'All Events', 'massaro' ),
		'add_new_item'          => __( 'Add New Event', 'massaro' ),
		'add_new'               => __( 'Add New', 'massaro' ),
		'new_item'              => __( 'New Event', 'massaro' ),
		'edit_item'             => __( 'Edit Event', 'massaro' ),
		'update_item'           => __( 'Update Event', 'massaro' ),
		'view_item'             => __( 'View Event', 'massaro' ),
		'view_items'            => __( 'View Events', 'massaro' ),
		'search_items'          => __( 'Search Event', 'massaro' ),
		'not_found'             => __( 'Not found', 'massaro' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'massaro' ),
		'featured_image'        => __( 'Featured Image', 'massaro' ),
		'set_featured_image'    => __( 'Set featured image', 'massaro' ),
		'remove_featured_image' => __( 'Remove featured image', 'massaro' ),
		'use_featured_image'    => __( 'Use as featured image', 'massaro' ),
		'insert_into_item'      => __( 'Insert into event', 'massaro' ),
		'uploaded_to_this_item' => __( 'Uploaded to this event', 'massaro' ),
		'items_list'            => __( 'Events list', 'massaro' ),
		'items_list_navigation' => __( 'Events list navigation', 'massaro' ),
		'filter_items_list'     => __( 'Filter events list', 'massaro' ),
	);
	$args = array(
		'label'                 => __( 'Event', 'massaro' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt', 'author' ),
		'taxonomies'						=> array( 'categories-events', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-calendar-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'show_in_rest' => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'events', $args );

}
add_action( 'init', 'events', 0 );


// Register Webinars Post Type
function webinars() {

	$labels = array(
		'name'                  => _x( 'Webinars', 'Post Type General Name', 'massaro' ),
		'singular_name'         => _x( 'Webinar', 'Post Type Singular Name', 'massaro' ),
		'menu_name'             => __( 'Webinars', 'massaro' ),
		'name_admin_bar'        => __( 'Webinar', 'massaro' ),
		'archives'              => __( 'Webinar Archives', 'massaro' ),
		'attributes'            => __( 'Webinar Attributes', 'massaro' ),
		'parent_item_colon'     => __( 'Parent Webinar:', 'massaro' ),
		'all_items'             => __( 'All Webinars', 'massaro' ),
		'add_new_item'          => __( 'Add New Webinar', 'massaro' ),
		'add_new'               => __( 'Add New', 'massaro' ),
		'new_item'              => __( 'New Webinar', 'massaro' ),
		'edit_item'             => __( 'Edit Webinar', 'massaro' ),
		'update_item'           => __( 'Update Webinar', 'massaro' ),
		'view_item'             => __( 'View Webinar', 'massaro' ),
		'view_items'            => __( 'View Webinars', 'massaro' ),
		'search_items'          => __( 'Search Webinar', 'massaro' ),
		'not_found'             => __( 'Not found', 'massaro' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'massaro' ),
		'featured_image'        => __( 'Featured Image', 'massaro' ),
		'set_featured_image'    => __( 'Set featured image', 'massaro' ),
		'remove_featured_image' => __( 'Remove featured image', 'massaro' ),
		'use_featured_image'    => __( 'Use as featured image', 'massaro' ),
		'insert_into_item'      => __( 'Insert into webinar', 'massaro' ),
		'uploaded_to_this_item' => __( 'Uploaded to this webinar', 'massaro' ),
		'items_list'            => __( 'Webinars list', 'massaro' ),
		'items_list_navigation' => __( 'Items webinar navigation', 'massaro' ),
		'filter_items_list'     => __( 'Filter webinars list', 'massaro' ),
	);
	$args = array(
		'label'                 => __( 'Webinar', 'massaro' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt', 'author' ),
		'taxonomies'						=> array( 'categories-webinars', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-laptop',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'show_in_rest' => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'webinars', $args );

}
add_action( 'init', 'webinars', 0 );


// Register Resources Post Type
function resources() {

	$labels = array(
		'name'                  => _x( 'Resources', 'Post Type General Name', 'massaro' ),
		'singular_name'         => _x( 'Resource', 'Post Type Singular Name', 'massaro' ),
		'menu_name'             => __( 'Resources', 'massaro' ),
		'name_admin_bar'        => __( 'Resource', 'massaro' ),
		'archives'              => __( 'Resource Archives', 'massaro' ),
		'attributes'            => __( 'Resource Attributes', 'massaro' ),
		'parent_item_colon'     => __( 'Parent Resource:', 'massaro' ),
		'all_items'             => __( 'All Resources', 'massaro' ),
		'add_new_item'          => __( 'Add New Resource', 'massaro' ),
		'add_new'               => __( 'Add New', 'massaro' ),
		'new_item'              => __( 'New Resource', 'massaro' ),
		'edit_item'             => __( 'Edit Resource', 'massaro' ),
		'update_item'           => __( 'Update Resource', 'massaro' ),
		'view_item'             => __( 'View Resource', 'massaro' ),
		'view_items'            => __( 'View Resources', 'massaro' ),
		'search_items'          => __( 'Search Resource', 'massaro' ),
		'not_found'             => __( 'Not found', 'massaro' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'massaro' ),
		'featured_image'        => __( 'Featured Image', 'massaro' ),
		'set_featured_image'    => __( 'Set featured image', 'massaro' ),
		'remove_featured_image' => __( 'Remove featured image', 'massaro' ),
		'use_featured_image'    => __( 'Use as featured image', 'massaro' ),
		'insert_into_item'      => __( 'Insert into resource', 'massaro' ),
		'uploaded_to_this_item' => __( 'Uploaded to this resource', 'massaro' ),
		'items_list'            => __( 'Resources list', 'massaro' ),
		'items_list_navigation' => __( 'Resources list navigation', 'massaro' ),
		'filter_items_list'     => __( 'Filter resources list', 'massaro' ),
	);
	$args = array(
		'label'                 => __( 'Resource', 'massaro' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt', 'author' ),
		'taxonomies'						=> array( 'categories-resources', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-analytics',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'show_in_rest' => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'resources', $args );

}
add_action( 'init', 'resources', 0 );


// Register Case Studies Post Type
function case_studies() {

	$labels = array(
		'name'                  => _x( 'Case Studies', 'Post Type General Name', 'massaro' ),
		'singular_name'         => _x( 'Case Study', 'Post Type Singular Name', 'massaro' ),
		'menu_name'             => __( 'Case Studies', 'massaro' ),
		'name_admin_bar'        => __( 'Case Study', 'massaro' ),
		'archives'              => __( 'Case Study Archives', 'massaro' ),
		'attributes'            => __( 'Case Study Attributes', 'massaro' ),
		'parent_item_colon'     => __( 'Parent Case Study:', 'massaro' ),
		'all_items'             => __( 'All Case Studies', 'massaro' ),
		'add_new_item'          => __( 'Add New Case Study', 'massaro' ),
		'add_new'               => __( 'Add New', 'massaro' ),
		'new_item'              => __( 'New Case Study', 'massaro' ),
		'edit_item'             => __( 'Edit Case Study', 'massaro' ),
		'update_item'           => __( 'Update Case Study', 'massaro' ),
		'view_item'             => __( 'View Case Study', 'massaro' ),
		'view_items'            => __( 'View Case Studies', 'massaro' ),
		'search_items'          => __( 'Search Case Study', 'massaro' ),
		'not_found'             => __( 'Not found', 'massaro' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'massaro' ),
		'featured_image'        => __( 'Featured Image', 'massaro' ),
		'set_featured_image'    => __( 'Set featured image', 'massaro' ),
		'remove_featured_image' => __( 'Remove featured image', 'massaro' ),
		'use_featured_image'    => __( 'Use as featured image', 'massaro' ),
		'insert_into_item'      => __( 'Insert into item', 'massaro' ),
		'uploaded_to_this_item' => __( 'Uploaded to this case study', 'massaro' ),
		'items_list'            => __( 'Case studies list', 'massaro' ),
		'items_list_navigation' => __( 'Case studies list navigation', 'massaro' ),
		'filter_items_list'     => __( 'Filter case studies list', 'massaro' ),
	);
	$rewrite = array(
		'slug'                  => 'case-study',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => false,
	);
	$args = array(
		'label'                 => __( 'Case Study', 'massaro' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt', 'author' ),
		'taxonomies'						=> array( 'categories-case-study', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-text-page',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'show_in_rest' => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'case_study', $args );

}
add_action( 'init', 'case_studies', 0 );


// Register Job listings Post Type
function job_listings() {

	$labels = array(
		'name'                  => _x( 'Job Listings', 'Post Type General Name', 'massaro' ),
		'singular_name'         => _x( 'Job Listings', 'Post Type Singular Name', 'massaro' ),
		'menu_name'             => __( 'Job Listings', 'massaro' ),
		'name_admin_bar'        => __( 'Job Listings', 'massaro' ),
		'archives'              => __( 'Job Listings Archives', 'massaro' ),
		'attributes'            => __( 'Job Listings Attributes', 'massaro' ),
		'parent_item_colon'     => __( 'Parent Job Listings:', 'massaro' ),
		'all_items'             => __( 'All Job Listings', 'massaro' ),
		'add_new_item'          => __( 'Add New Job Listings', 'massaro' ),
		'add_new'               => __( 'Add New', 'massaro' ),
		'new_item'              => __( 'New Job Listings', 'massaro' ),
		'edit_item'             => __( 'Edit Job Listings', 'massaro' ),
		'update_item'           => __( 'Update Job Listings', 'massaro' ),
		'view_item'             => __( 'View Job Listings', 'massaro' ),
		'view_items'            => __( 'View Job Listings', 'massaro' ),
		'search_items'          => __( 'Search Job Listings', 'massaro' ),
		'not_found'             => __( 'Not found', 'massaro' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'massaro' ),
		'featured_image'        => __( 'Featured Image', 'massaro' ),
		'set_featured_image'    => __( 'Set featured image', 'massaro' ),
		'remove_featured_image' => __( 'Remove featured image', 'massaro' ),
		'use_featured_image'    => __( 'Use as featured image', 'massaro' ),
		'insert_into_item'      => __( 'Insert into Job Listings', 'massaro' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Job Listings', 'massaro' ),
		'items_list'            => __( 'Job Listings list', 'massaro' ),
		'items_list_navigation' => __( 'Job Listings list navigation', 'massaro' ),
		'filter_items_list'     => __( 'Filter Job Listings list', 'massaro' ),
	);
	$args = array(
		'label'                 => __( 'Job Listings', 'massaro' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-analytics',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'show_in_rest' => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'template' =>  array(
			array( 'core/columns', array(), array(
				array( 'core/column', array(), array(
					array( 'core/paragraph', array('content' => 'Job Posting', 'class' => 'text-uppercase' ) ),
					array( 'core/heading', array( 'level' => '3', 'content' => 'You could be our next' ) ),
					array( 'core/heading', array( 'level' => '2', 'placeholder' => 'Enter position' ) ),
					array( 'core/image', array('url' => 'http://massaro.hswp.net/content/uploads/2019/12/three-dots.png') ),
					) ),
				array( 'core/column', array(), array(
					array( 'core/paragraph', array('placeholder' => 'Enter opportunities' ) ),
					array( 'core/paragraph', array( 'placeholder' => 'Enter opportunities' ) ),
					) ),
				),
			), 
		),
	);
	register_post_type( 'job-listings', $args );
}
add_action( 'init', 'job_listings', 0 );

?>