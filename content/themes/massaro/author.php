<?php

get_header(); ?>

<header>
  <?php
    $author = get_user_by( 'slug', get_query_var( 'author_name' ) );

    $first_name = get_the_author_meta('first_name', $author->ID);
    $last_name = get_the_author_meta('last_name', $author->ID);
    if ( !empty($first_name) && !empty($last_name) ) {
      $full_name = $first_name . ' ' . $last_name;
    } else {
      $full_name = get_the_author_meta('display_name', $author->ID);
    }

    $job_position = get_field('users_job_position', 'user_'.$author->ID);
    $user_image = get_field('users_image', 'user_'.$author->ID);
    $users_about = get_field('users_about', 'user_'.$author->ID);
    $linkedin_profile = get_field('users_linkedin_profile', 'user_'.$author->ID);
    $sitebar_text = get_field('users_sitebar_text', 'user_'.$author->ID);
    $areas_of_expertise = get_field('users_areas_of_expertise', 'user_'.$author->ID);
  ?>
  
	<section class="author-hero g-hero hero">
		<div class="grid-container">
			<div class="g-hero-inner grid-x">
				<div class="cell small-12 medium-6">
          <h1><?php echo $full_name; ?></h1>
          <?php if ( $job_position ) { ?>
            <h4><?php echo $job_position; ?></h4>
          <?php } ?>
        </div>
        <div class="cell small-12 medium-6 author-img">
          <?php if ($user_image) { ?>          
          <img src="<?php echo $user_image['url']; ?>" alt="Author Image">
          <?php } ?>
				</div>
			</div>
		</div>
	</section>
</header>

<div class="grid-container author-content">
	<div class="grid-x grid-margin-x">
		<main class="cell small-12 medium-8">
      <?php 
      if ( $users_about ) {
        echo $users_about; 
      }
      ?>
    </main>
    <aside class="cell small-12 medium-3 medium-offset-1 author-sitebar">
      <section class="contact">
        <?php if ( !empty($first_name) ) { ?>
          <h4 class="sitebar-section-title"><?php echo __('Contact', 'massaro') . ' ' . $first_name; ?></h4>
        <?php } else { ?>
          <h4 class="sitebar-section-title"><?php echo __('Contact', 'massaro') . ' ' . $full_name; ?></h4>
        <?php } ?>

        <?php if ( $linkedin_profile ) { ?>
          <a class="linkedin-profile" href="<?php echo $linkedin_profile; ?>">LinkedIn Profile</a>
        <?php } ?>

        <?php $user_email = get_the_author_meta('user_email', $author->ID); ?>
        <a class="email" href="mailto:<?php echo $user_email; ?>"><?php _e ('Send an Email', 'massaro')?></a>
      </section>

      <?php if ( $sitebar_text ) { ?>
        <section>
          <?php if ( !empty($first_name) ) { ?>
            <h4 class="sitebar-section-title"><?php echo $first_name . '\'s' . ' ' . 'Massaro'; ?></h4>
          <?php } else { ?>
            <h4 class="sitebar-section-title"><?php echo $full_name . '\'s' . ' ' . 'Massaro'; ?></h4>
          <?php } ?>

          <?php echo $sitebar_text; ?>
        </section>
      <?php } ?>


      <?php if ( $areas_of_expertise ) { ?>
        <section>
          <h4 class="sitebar-section-title"><?php _e('Areas of Expertise', 'massaro'); ?></h4>
          <?php echo $areas_of_expertise; ?>
        </section>
      <?php } ?>
    </aside>
  </div>

  <?php
    $args = array(
      'author'        =>  $author->ID,
      'orderby'       =>  'post_date',
      'order'         =>  'ASC',
      'posts_per_page' => 3
    );
    
    $the_query = new WP_Query( $args );
    
    // The Loop
    if ( $the_query->have_posts() ) { ?>
      <div class="latest-posts-container">
        <?php if ( !empty($first_name) ) { ?>
          <h4 class="latest-posts-title"><?php echo $first_name . '\'s' . ' ' . 'Latest Posts'; ?></h4>
        <?php } else { ?>
          <h4 class="latest-posts-title"><?php echo $full_name . '\'s' . ' ' . 'Latest Posts'; ?></h4>
        <?php } ?>


        <div class="grid-x grid-margin-x small-up-1 medium-up-3">
          <?php
          while ( $the_query->have_posts() ) {
            $the_query->the_post();
            get_template_part( 'template-parts/listing', 'resources' );
          } ?>
        </div>
      </div>
      <?php
    }
  
    /* Restore original Post Data */
    wp_reset_postdata(); ?>
</div>
<?php get_footer();