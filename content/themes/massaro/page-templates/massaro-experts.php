<?php
/*
Template Name: Massaro Experts
*/
get_header(); ?>

<?php
// WP_User_Query arguments
$args = array (
  'meta_query' => array(
    array(
      'key'     => 'users_team',
      'value'   => '2',
    ),
  )
);

// Create the WP_User_Query object
$wp_user_query = new WP_User_Query($args);

// Get the results
$authors = $wp_user_query->get_results();
?>

<main class="main-content-full-width">
  <?php if (!empty($authors)) { ?>
    <div class="massaro-experts">
      <div class="massaro-experts-inner">
        <?php foreach ($authors as $author) { ?>
          <?php
          $team_member_image = get_field('users_team_member_image', 'user_'.$author->ID);
          $job_position = get_field('users_job_position', 'user_'.$author->ID);
          $linkedin_profile = get_field('users_linkedin_profile', 'user_'.$author->ID);

          $author_info = get_userdata($author->ID);

          if ( !empty($author_info->first_name) && !empty($author_info->last_name) ) {
            $full_name = $author_info->first_name . ' ' . $author_info->last_name;
          } else {
            $full_name = $author_info->display_name;
          } ?>

          <div class="team-member">
            <?php if ( $team_member_image ) { ?>
              <div class="team-member-img" style="background-image: url('<?php echo $team_member_image['sizes']['medium']; ?>')"></div>
            <?php } ?>
            <h5 class="team-member-name"><?php echo $full_name; ?></h5>
            <?php if ( $job_position ) { ?>
              <p class="team-member-job-position"><?php echo wp_trim_words($job_position, 10, '...'); ?></p>
            <?php } ?>

            <div class="links">
              <?php if ( $linkedin_profile ) { ?>
                <a class="team-member-linkedin" href="<?php echo $linkedin_profile; ?>"></a>
              <?php } ?>

              <a href="<?php echo get_author_posts_url($author->ID); ?>" class="button button-arrow success"><?php _e('Read full bio', 'massaro'); ?></a>
            </div>

          </div>

        <?php } ?>
      </div>
    </div>
  <?php } ?>
</main>
<?php get_footer();