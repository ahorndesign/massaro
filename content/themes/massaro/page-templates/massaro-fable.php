<?php
/*
Template Name: Massaro Fable
*/
get_header(); ?>

<main class="main-content-full-width is-style-grid-lblue-background main-content-massaro-fable">
	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'template-parts/content', 'page' ); ?>
		<?php comments_template(); ?>
	<?php endwhile; ?>
</main>
<?php get_footer();
