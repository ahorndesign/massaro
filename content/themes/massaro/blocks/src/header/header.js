/**
 * BLOCK: blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType, Component } = wp.blocks; // Import registerBlockType() from wp.blocks
const {
	InnerBlocks,
	InspectorControls,
	PlainText,
	RichText,
	MediaUpload,
	MediaUploadCheck,
} = wp.blockEditor;
const {
	PanelBody,
	Button,
	RadioControl
} = wp.components;
const {
	withState
} = wp.compose;
const TEMPLATE = [
	['core/paragraph', { placeholder: 'Enter a description' }],
];

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType('hlwp/header', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __('Header'), // Block title.
	icon: 'feedback', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__('header'), __('hero'),
	],
	attributes: {
		option: {
			type: 'string',
			default: 'disabled',
		},
		mediaID: {
			type: 'number',
		},
		mediaURL: {
			type: 'string'
		},
		mediaAlt: {
			type: 'string',
			source: 'attribute',
			selector: 'img',
			attribute: 'alt',
		},

	},


	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	edit: function (props) {
		const { attributes: {
			mediaID,
			mediaURL,
			mediaAlt,
			option
		},
			className,
			setAttributes } = props;

		const onSelectImage = (media) => {
			props.setAttributes({
				mediaURL: media.url,
				mediaID: media.id,
				mediaAlt: media.alt,
			});
		};

		const style = {
			backgroundImage: `url(${mediaURL})`,
		}
		const overlayBck = {
			backgroundImage: `url(${window.global.themeurl}/dist/assets/images/slicing/home/hue_new.png)`,
		}
		const controls = [
			<InspectorControls>
				<PanelBody
					title={__('Image')}
					initialOpen={true}
				>
					{mediaURL &&
						<div>
							<img src={mediaURL} alt={mediaAlt} />
						</div>
					}

					<div>
						<MediaUploadCheck>
							<MediaUpload
								onSelect={onSelectImage}
								allowedTypes="image"
								value={mediaID}
								render={({ open }) => (
									<Button className={'button button-large'} onClick={open}>
										{!mediaID ? 'Upload Image' : 'Change Image'}
									</Button>
								)}
							/>
						</MediaUploadCheck>
						{mediaID && <Button className={'button button-large'} onClick={onSelectImage}>Remove Image</Button>}
					</div>
				</PanelBody>
			</InspectorControls>
		];
		return (
			<section className="g-hero g-hero-admin hero" style={style}>
				<div className="g-hero-inner grid-x">
					<div className="cell small-12 medium-6">
						{controls}
						<InnerBlocks
							allowedBlocks={['core/paragraph', 'core/heading', 'core/button', 'core/group']}
						/>
					</div>
					<div className="cell small-12 medium-6"></div>
				</div>
			</section>
		)
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	save: function (props) {
		const {
			mediaID,
			mediaURL,
			mediaAlt,
			option,
			className,
		} = props.attributes;

		const classNameInner = `callout ${props.attributes.option}`;
		const style = {
			backgroundImage: `url(${mediaURL})`,
		}

		return (

			<section className="g-hero hero" style={style}>
				<div className="grid-container">
					<div className="g-hero-inner grid-x">
						<div className="cell small-12 medium-6">
							<InnerBlocks.Content />
						</div>
						<div className="cell small-12 medium-6"></div>
					</div>
				</div>
			</section>
		)

	},
});
