/**
 * BLOCK: blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType, Component } = wp.blocks; // Import registerBlockType() from wp.blocks
const {
	InspectorControls
} = wp.editor;
const {
	PanelBody,
	TextControl,
	TextareaControl,
	ToggleControl,
	BaseControl
} = wp.components;
const {
	withState
} = wp.compose;

const {
    Fragment
} = wp.element;

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'hswp/link-box', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Link Box' ), // Block title.
	icon: 'laptop', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'tablet' ), __( 'section' ),
	],
	attributes: {		
		
		title: {
			type: 'string',
			source: 'text',
			selector: 'h3',
		},
		text: {
			type: 'string',
			source: 'text',
			selector: 'p',
		},
		link: {
			type: 'string',
			source: 'attribute',
			attribute: 'href',
			selector: 'a.link-box',
		},

		iframeURL: {
			type: 'string',
			source: 'attribute',
			attribute: 'src',
			selector: '.reveal iframe.reveal-iframe',
		},
		isModal: {
			type: 'boolean',
			default: false
			
		}
	},


	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	edit: function( props ) {
		const { attributes: { 
					title,
					text,
					link,
					isModal
				}, 
    		className,
    		setAttributes } = props;
		
		const onSelectImage = ( media ) => {
			props.setAttributes( {
				mediaURL: media.url,
				mediaID: media.id,
				mediaAlt: media.alt,
			} );
		};		
		
		const classNameInner = `grid-x grid-margin-x backend-tablet-section-preview ${className}`;
		
		const controls = [
			<BaseControl>
				<TextControl
					label="Title"
					value={ title }
					onChange={ ( title ) => props.setAttributes( {'title': title} ) }
				/>
				<TextareaControl
					label="Text"
					value={ text }
					onChange={ ( text ) => props.setAttributes( {'text': text} )  }
				/>
			</BaseControl>											
		];

		const settins = [
			<InspectorControls>
				<PanelBody
					title={ __( 'General Settings' ) }
					initialOpen={ true }
				>
					<TextControl
						label="Link"
						value={ link }
						onChange={ ( link ) => { 
							props.setAttributes( {'link': link} ) 
							props.setAttributes( {'iframeURL': link} ) 
						}}
					/>
					<ToggleControl
						label="Open in modal"
						help={ isModal ? 'The link will be opened in a modal' : 'Direct Link' }
						checked={ isModal }
						onChange={ () => props.setAttributes( {'isModal': !isModal, 'link': link} ) }
					/>
				</PanelBody>
			</InspectorControls>										
		];

	    return (
	        <div className="box-link">
				  {controls}
				  {settins}
	        </div>
        )		               
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	save: function( props ) {
		const {
					title,
					text,
					link,
					isModal,
					iframeURL
				} = props.attributes;

		const slug = slugify(title);
		
		const modal = (isModal) ? (
			<div className="reveal large" id={slug} data-reveal>
				<iframe className="reveal-iframe" src={link}></iframe>
				<button class="close-button" data-close="" aria-label="Close modal" type="button">
					<span aria-hidden="true">×</span>
					</button>
			</div>
		): null;

		const modalDataAttribute = ( (isModal) ? {"data-open": slug} : {})
	    return (
			<Fragment>
				<div className="link-box-outer arrow-block">
					<a className="link-box"  {...modalDataAttribute} href={ link } onclick={ (event) => {
						if(link && !isModal)event.preventDefault()
					}}>
						<div className="hover-arrow"></div>
						<div className="content-box">
							<h3>{title}</h3>
							<p>{text}</p>
						</div>
					</a>
				</div>
			{modal}
			</Fragment>
        )			
	},
} );


function slugify(string) {
	const a = 'àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;'
	const b = 'aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnooooooooprrsssssttuuuuuuuuuwxyyzzz------'
	const p = new RegExp(a.split('').join('|'), 'g')
  
	return string.toString().toLowerCase()
	  .replace(/\s+/g, '-') // Replace spaces with -
	  .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
	  .replace(/&/g, '-and-') // Replace & with 'and'
	  .replace(/[^\w\-]+/g, '') // Remove all non-word characters
	  .replace(/\-\-+/g, '-') // Replace multiple - with single -
	  .replace(/^-+/, '') // Trim - from start of text
	  .replace(/-+$/, '') // Trim - from end of text
  }