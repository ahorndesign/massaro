// wp.blocks.registerBlockStyle( 'core/quote', {
//     name: 'fancy-quote',
//     label: 'Fancy Quote'
// } );


// wp.blocks.registerBlockStyle( 'core/gallery', {
//     name: 'default-style',
//     label: 'Default',
//     isDefault: true
// } );

// wp.blocks.registerBlockStyle( 'core/gallery', {
//     name: 'owl-carousel',
//     label: 'OWL Carousel'
// } );

// wp.blocks.registerBlockStyle( 'core/image', {
//     name: 'default-style',
//     label: 'Default',
//     isDefault: true
// } );

// wp.blocks.registerBlockStyle( 'core/image', {
//     name: 'popout-image',
//     label: 'Pop-out Image'
// } );


// function addBackgroundColorStyle( props,blockType ) {

// 	if(blockType.name === 'core/gallery' && props.className.indexOf('is-style-owl-carousel') !== -1 ) {
//     	    	return Object.assign( props, { className: props.className+' owl-carousel owl-theme' } );    	
// 	}
// 	return props;
// }

// wp.hooks.addFilter(
//     'blocks.getSaveContent.extraProps',
//     'my-plugin/add-background-color-style',
//     addBackgroundColorStyle
// );



// function getBlockSettings( settings, blockName ) {
//     return settings;
// }

// wp.hooks.addFilter(
//     'blocks.registerBlockType',
//     'core/gallery',
//     getBlockSettings
// );

wp.blocks.registerBlockStyle( 'hlwp/header', {
    name: 'default-style',
    label: 'Default',
    isDefault: true
} );

wp.blocks.registerBlockStyle( 'hlwp/header', {
    name: 'has-overlay',
    label: 'With Overlay' 
} );

wp.blocks.registerBlockStyle( 'hlwp/header', {
    name: 'lower-height-overlay',
    label: 'Lower Height Header'
} );

wp.blocks.registerBlockStyle( 'core/heading', {
    name: 'default-style',
    label: 'Default',
    isDefault: true
} );

wp.blocks.registerBlockStyle( 'core/heading', {
    name: 'green',
    label: 'Green',
} );

wp.blocks.registerBlockStyle( 'core/list', {
    name: 'default-style',
    label: 'Default',
    isDefault: true
} );

wp.blocks.registerBlockStyle( 'core/list', {
    name: 'yellow',
    label: 'Yellow OL' 
} );

wp.blocks.registerBlockStyle( 'core/list', {
    name: 'white',
    label: 'White OL' 
} );

wp.blocks.registerBlockStyle( 'core/list', {
    name: 'blue',
    label: 'Blue OL' 
} );

wp.blocks.registerBlockStyle( 'core/list', {
    name: 'cyan',
    label: 'Cyan OL' 
} );

wp.blocks.registerBlockStyle( 'core/list', {
    name: 'green',
    label: 'Green UL' 
} );

wp.blocks.registerBlockStyle( 'core/button', {
    name: 'arrow-down',
    label: 'Arrow Down' 
} );

wp.blocks.registerBlockStyle( 'core/button', {
    name: 'arrow-reverse',
    label: 'Reverse Arrow' 
} );


wp.blocks.registerBlockStyle( 'core/image', {
    name: 'stretch',
    label: 'Stretch' 
} );


wp.blocks.registerBlockStyle( 'core/group', {
    name: 'grid-green-background',
    label: 'Grid Green Background' 
} );

wp.blocks.registerBlockStyle( 'core/group', {
    name: 'grid-lblue-background',
    label: 'Grid Light Blue Background' 
} );

wp.blocks.registerBlockStyle( 'core/group', {
    name: 'grid-turquoise-background',
    label: 'Grid Turquoise Background' 
} );

wp.blocks.registerBlockStyle( 'core/group', {
    name: 'center-inside-group',
    label: 'Center inside group' 
} );

wp.blocks.registerBlockStyle( 'core/image', {
    name: 'popout-left',
    label: 'Popout left' 
} );


wp.blocks.registerBlockStyle( 'core/paragraph', {
    name: 'green-paragraph',
    label: 'Green paragraph' 
} );

wp.blocks.registerBlockStyle( 'hlwp/header', {
    name: 'contain-image',
    label: 'Contain Image' 
} );