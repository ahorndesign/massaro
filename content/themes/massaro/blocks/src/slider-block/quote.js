const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType, Component } = wp.blocks; // Import registerBlockType() from wp.blocks
import { Fragment } from '@wordpress/element';

const {
	InnerBlocks,
	InspectorControls,
	MediaUpload,
	MediaUploadCheck,
	RichText,
	BlockControls
} = wp.editor;
const {
	PanelBody,
	TextControl,
	TextareaControl,
	BaseControl,
	RangeControl,
	SelectControl,
	Placeholder,
	Button
} = wp.components;
import { join, split, create, toHTMLString } from '@wordpress/rich-text';
import { G, Path, SVG } from '@wordpress/components';
const ATTRIBUTE_QUOTE = 'value';
const ATTRIBUTE_CITATION = 'citation';

	registerBlockType( 'hswp/quote', {
		// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
		title: __( 'Quote' ), // Block title.
		icon: <SVG viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><Path fill="none" d="M0 0h24v24H0V0z" /><G><Path d="M19 18h-6l2-4h-2V6h8v7l-2 5zm-2-2l2-3V8h-4v4h4l-2 4zm-8 2H3l2-4H3V6h8v7l-2 5zm-2-2l2-3V8H5v4h4l-2 4z" /></G></SVG>, // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
		category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
		keywords: [
			__( 'quote' ), __( 'citation' ),
		],
		attributes: {
			mediaID: {
				type: 'number',
			},
			mediaURL: {
				type: 'string',
				source: 'attribute',
				selector: 'img',
				attribute: 'src',
			},
			mediaAlt: {
				type: 'string',
				source: 'attribute',
				selector: 'img',
				attribute: 'alt',
			},
			[ ATTRIBUTE_QUOTE ]: {
				type: 'string',
				source: 'html',
				selector: 'blockquote',
				multiline: 'p',
				default: '',
			},
			[ ATTRIBUTE_CITATION ]: {
				type: 'string',
				source: 'html',
				selector: 'cite',
				default: '',
			},
			align: {
				type: 'string',
			},
		},
		styles: [
			{ name: 'default', label: __( 'Default Image', 'hswp' ), isDefault: true },
			{ name: 'round-avatar', label: __( 'Circular Image', 'hswp' ) },
		],

		/**
		 * The edit function describes the structure of your block in the context of the editor.
		 * This represents what the editor will render when the block is used.
		 *
		 * The "edit" property must be a valid function.
		 *
		 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
		 */

		

		edit( { attributes, setAttributes, isSelected, mergeBlocks, onReplace, className } ) {
			
			const { align, value, citation, mediaID, mediaURL, mediaAlt } = attributes;
			
			const onSelectImage = ( media ) => {
				setAttributes( {
					mediaURL: media.url,
					mediaID: media.id,
					mediaAlt: media.alt,
				} );
			};	

			const controls = [
				<InspectorControls>
					<PanelBody
						title={ __( 'Link' ) }
						initialOpen={ true }
					>
						<div>
							<MediaUploadCheck>
								<MediaUpload
										onSelect={ onSelectImage }
										allowedTypes="image"
										value={ mediaID }
										render={ ( { open } ) => (
											<Button className={ mediaID ? 'image-button' : 'button button-large' } onClick={ open }>
												{ ! mediaID ? __( 'Upload Image', 'gutenberg-examples' ) : <img src={ mediaURL } alt={ __( 'Upload Recipe Image', 'gutenberg-examples' ) } /> }
											</Button>									
										) }
									/>
							</MediaUploadCheck>
						</div>
						<div>
							{mediaID && <Button className={ 'button button-large' } onClick={ onSelectImage }>Remove Image</Button> }
						</div>											
					</PanelBody>
				</InspectorControls>    
			];


			return (
				<Fragment>					
					<div className="grid-x grid-margin-x blockquote-block">
					    {controls}
					    { mediaURL && 
						    <div className="cell medium-2 quote-avatar">
						    	<div className="quotes-avatar-border">
							    	<img src={mediaURL} alt={mediaAlt} className="quote-avatar-image" />
						    	</div>
						    </div> }
						<blockquote className="cell auto">
						{ ( ! RichText.isEmpty( citation ) || isSelected ) && (
								<RichText
									tagName="cite"
									identifier={ ATTRIBUTE_CITATION }
									value={ citation }
									onChange={
										( nextCitation ) => setAttributes( {
											citation: nextCitation,
										} )
									}
									placeholder={
										// translators: placeholder text used for the citation
										__( 'Write citation…' )
									}
									className="wp-block-quote__citation"
								/>
							) }
							<RichText
								identifier={ ATTRIBUTE_QUOTE }
								multiline
								value={ value }
								onChange={
									( nextValue ) => setAttributes( {
										value: nextValue,
									} )
								}
								onMerge={ mergeBlocks }
								onRemove={ ( forward ) => {
									const hasEmptyCitation = ! citation || citation.length === 0;
									if ( ! forward && hasEmptyCitation ) {
										onReplace( [] );
									}
								} }
								placeholder={
									// translators: placeholder text used for the quote
									__( 'Write quote…' )
								}
							/>
							
						</blockquote>
					</div>
				</Fragment>
			);
		},


		/**
		 * The save function defines the way in which the different attributes should be combined
		 * into the final markup, which is then serialized by Gutenberg into post_content.
		 *
		 * The "save" property must be specified and must be a valid function.
		 *
		 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
		 */
		save: function( props ) {
			const { align, value, citation, mediaURL, mediaAlt } = props.attributes;

			return (
				<blockquote class="wp-block-quote">
					{mediaURL && 
					    	<div className="quotes-avatar-border">
						    	<img src={mediaURL} alt={mediaAlt} className="quote-avatar-image" />
					    	</div>
					}	
						<RichText.Content multiline value={ value } />
						{ ! RichText.isEmpty( citation ) && <RichText.Content tagName="cite" value={ citation } /> }
				</blockquote>
			);
		},
		merge( attributes, { value, citation } ) {
			if ( ! value || value === '<p></p>' ) {
				return {
					...attributes,
					citation: attributes.citation + citation,
				};
			}

			return {
				...attributes,
				value: attributes.value + value,
				citation: attributes.citation + citation,
			};
		},
	} );
