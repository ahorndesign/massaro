/**
 * BLOCK: blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType, Component } = wp.blocks; // Import registerBlockType() from wp.blocks
const {
	InnerBlocks,
	RichText
} = wp.editor;
const TEMPLATE = [
	[ 'hswp/quote' ],
	[ 'hswp/quote' ],
	[ 'hswp/quote' ]
];

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'hswp/quotes-slider', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Quotes Slider' ), // Block title.
	icon: 'list-view', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'slider' ), __( 'qoutes' )
	],
	attributes: {
		title: {
            type: 'string',
		},	
	},


	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	edit: function( props ) {
		const { attributes: { 
			title,
			}, 
		setAttributes } = props;
	    return (
			<div className="g-quotes-slider">
			<RichText
				tagName="h2"
				placeholder={ __( 'Here goes the title', 'hlwp' ) }
				onChange={ title => { setAttributes( { title } ) } }
				value={ title }
			/>   

			<InnerBlocks
					template={ TEMPLATE }
					allowedBlocks={[ 'hswp/quote']}
				/>
		</div>
	       
        )		               
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	save: function( props ) {
		const { attributes: { 
			title,
			}, 
		setAttributes } = props;

	    return (
			<div className="g-quotes-outer">
					<div className="grid-x">
						<div className="cell small-12 medium-3">
							<h2 className="h4">{title}</h2>
						</div>
					</div>
					<div className="g-quotes-slider grid-x grid-margin-x">
						<div className="cell small-12 medium-1">
						</div>
						<div className="cell small-12 medium-auto">
							<div className="owl-carousel owl-quotes owl-theme">
								<InnerBlocks.Content/>
							</div>
						</div>
					</div>
				</div>
        )					
	},
} );
