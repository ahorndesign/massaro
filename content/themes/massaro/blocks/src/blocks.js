/**
 * Gutenberg Blocks
 *
 * All blocks related JavaScript files should be imported here.
 * You can create a new block folder in this dir and include code
 * for that block here as well.
 *
 * All blocks should be included here since this is the file that
 * Webpack is compiling as the input file.
 */

// import './block/block.js';
import './post-grid/post-grid.js';
// import './callout/callout.js';
import './header/header.js';
import './event-block/event-block.js';
import './slider-block/quotes-slider.js';
import './slider-block/quote.js';
import './fable-slider/slider-block.js';
import './fable-slider/slider-block-item.js';
import './link-boxes/link-boxes.js';
import './appliances/appliances.js';

// Filters
import './filters.js';
