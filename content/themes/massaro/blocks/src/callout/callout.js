/**
 * BLOCK: blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType, Component } = wp.blocks; // Import registerBlockType() from wp.blocks
const {
	InnerBlocks,
	InspectorControls,
	MediaUpload,
	MediaUploadCheck,
	PlainText,
	RichText,
} = wp.editor;
const {
	PanelBody,
	TextControl,
	Button,
	RadioControl
} = wp.components;
const {
	withState
} = wp.compose;
const TEMPLATE = [
	[ 'core/heading', { level: 2, placeholder: 'Enter a heading' } ],
	[ 'core/paragraph', { placeholder: 'Enter a description' } ],
	[ 'core/button', { placeholder: 'Enter button text' } ],

];

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'hack/callout', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Callout Block' ), // Block title.
	icon: 'feedback', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'callout' ),
	],
	attributes: {		
		mediaID: {
			type: 'number',
		},
		mediaURL: {
			type: 'string',
			source: 'attribute',
			selector: 'img',
			attribute: 'src',
		},
		mediaAlt: {
			type: 'string',
			source: 'attribute',
			selector: 'img',
			attribute: 'alt',
		},
		option: {
			type: 'string',
			default: 'no-image',
		},

	},


	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	edit: function( props ) {
		const { attributes: { 
					mediaID,
					mediaURL,
					mediaAlt,
					option
				}, 
    		className,
    		setAttributes } = props;
		
		const onSelectImage = ( media ) => {
			console.log(media)
			props.setAttributes( {
				mediaURL: media.url,
				mediaID: media.id,
				mediaAlt: media.alt,
			} );
		};		
		
		const classNameInner = `callout ${props.attributes.option} callout-box ${mediaURL ? 'callout-has-image' : 'callout-no-image' }`;
		
		const controls = [
			<InspectorControls>
				<RadioControl
					label="Image alignment"
					help="Default alignment - no-image"
					selected={ option }
					options={ [
						{ label: 'Left', value: 'image-left' },
						{ label: 'Right', value: 'image-right' },
						{ label: 'No image', value: 'no-image' },
					] }
					onChange={ ( option ) => {props.setAttributes( {'option': option}  ) } }
				/>
				<PanelBody
					title={ __( 'Link' ) }
					initialOpen={ true }
				>
					<div>
						<MediaUploadCheck>
							<MediaUpload
									onSelect={ onSelectImage }
									allowedTypes="image"
									value={ mediaID }
									render={ ( { open } ) => (
										<Button className={ mediaID ? 'image-button' : 'button button-large' } onClick={ open }>
											{ ! mediaID ? __( 'Upload Image', 'gutenberg-examples' ) : <img src={ mediaURL } alt={ __( 'Upload Recipe Image', 'gutenberg-examples' ) } /> }
										</Button>									
									) }
								/>
						</MediaUploadCheck>
					</div>
					<div>
						{mediaID && <Button className={ 'button button-large' } onClick={ onSelectImage }>Remove Image</Button> }
					</div>											
				</PanelBody>
			</InspectorControls>    
		];
		 
			return (
				<div className={classNameInner}>    
					{controls}
					{mediaURL && <img className="callout-icon-image" src={ mediaURL } alt={ mediaAlt } /> }
					<InnerBlocks
						template={ TEMPLATE }
						allowedBlocks={['core/paragraph', 'core/heading', 'core/button', 'kadence/rowlayout']}
					/>			
				</div>
			)	               
		},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	save: function( props ) {
		const {
					mediaID,
					mediaURL,
					mediaAlt,
					setAttributes,
					className,
				} = props.attributes;

		const classNameInner = `callout ${props.attributes.option} ${mediaURL ? 'callout-has-image' : 'callout-no-image' }`;

			
			return (
				 <div className={classNameInner}>    
					{mediaURL && <img className="callout-icon-image" src={ mediaURL } alt={ mediaAlt } /> }
					<InnerBlocks.Content/>
				</div>
	        ) 		
	},
} );
