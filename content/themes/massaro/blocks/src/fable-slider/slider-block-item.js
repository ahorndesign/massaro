/**
 * BLOCK: blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType, Component } = wp.blocks; // Import registerBlockType() from wp.blocks
const {
	InnerBlocks,
	InspectorControls,
	PlainText,
	RichText,
	MediaUpload,
	MediaUploadCheck,
} = wp.editor;
const {
	PanelBody,
	TextControl,
	Button,
} = wp.components;
const {
	withState
} = wp.compose;
const TEMPLATE = [
	['core/columns']
];

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'hlwp/slider-block-item', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Slider block item' ), // Block title.
	icon: 'list-view', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	parent: [ 'hlwp/slider-block' ],
	keywords: [
		__( 'Slider block, card, grid' ),
	],
	attributes: {		
		mediaID: {
			type: 'number',
		},
		mediaURL: {
			type: 'string'
		},
		mediaAlt: {
			type: 'string',
			source: 'attribute',
			selector: 'img',
			attribute: 'alt',
		}
	},


	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	edit: function( props ) {		
		const { attributes: { 
			mediaID,
			mediaURL,
			mediaAlt,
			}, 
		setAttributes } = props;

		const onSelectImage = ( media ) => {
			props.setAttributes( {
				mediaURL: media.url,
				mediaID: media.id,
				mediaAlt: media.alt,
			} );
		};	


		const classNameInner = `item`;
	    return (
            <div className={classNameInner}>
                <div className="slider-block-title">                
					<InnerBlocks
						template={ TEMPLATE }
						allowedBlocks={[ 'core/columns','core/paragraph','core/heading','core/button']}
					/>
				</div>
			</div>
        )		               
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	save: function( props ) {
		const {	
			mediaURL
		} = props.attributes;

		const style = {
			backgroundImage: `url(${mediaURL})`,
		}

		const classNameInner = `item`;
	    return (
			<div className={classNameInner}>	
				<div className="grid-x">
						<InnerBlocks.Content/>
				</div>			
			</div>
        )					
	},
} );
