<?php
/**
 * Blocks Initializer
 *
 * Enqueue CSS/JS of all the blocks.
 *
 * @since   1.0.0
 * @package CGB
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Enqueue Gutenberg block assets for both frontend + backend.
 *
 * @uses {wp-editor} for WP editor styles.
 * @since 1.0.0
 */
function blocks_cgb_block_assets() { // phpcs:ignore
	// Styles.
/*
	wp_enqueue_style(
		'blocks-cgb-style-css', // Handle.
		get_stylesheet_directory_uri(). '/blocks/dist/blocks.style.build.css', // Block style CSS.
		array( 'wp-editor' ) // Dependency to include the CSS after it.
		// filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.style.build.css' ) // Version: File modification time.
	);
*/
}

// Hook: Frontend assets.
add_action( 'enqueue_block_assets', 'blocks_cgb_block_assets' );

/**
 * Enqueue Gutenberg block assets for backend editor.
 *
 * @uses {wp-blocks} for block type registration & related functions.
 * @uses {wp-element} for WP Element abstraction — structure of blocks.
 * @uses {wp-i18n} to internationalize the block's text.
 * @uses {wp-editor} for WP editor styles.
 * @since 1.0.0
 */
function blocks_cgb_editor_assets() { // phpcs:ignore
	// Scripts.
	wp_enqueue_script(
		'blocks-cgb-block-js', // Handle.
		get_stylesheet_directory_uri(). '/blocks/dist/blocks.build.js', // Block.build.js: We register the block here. Built with Webpack.
		array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'wp-hooks', 'wp-data' ), // Dependencies, defined above.
		// filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.build.js' ), // Version: File modification time.
		true // Enqueue the script in the footer.
	);

	
    $templates=array(
		'content.php' => 'Choose a template',
		'listing-work.php' => 'Listing work',
		'listing-jobs.php' => 'Listing jobs',
		'massaro-experts.php' => 'Experts',
    );
    
	wp_localize_script('blocks-cgb-block-js', 'global', array(
		'post_types' => array('post' => "Posts", 'knowledge-base' => "Knowledge base"),
		'templates' => $templates,
	    'appliances' =>  get_terms( 'appliances', array(
			'hide_empty' => false,
		) ),
	    'themeurl' => get_stylesheet_directory_uri( ),
	));

	// Styles.
	wp_enqueue_style(
		'blocks-cgb-block-editor-css', // Handle.
		get_stylesheet_directory_uri(). '/blocks/dist/blocks.editor.build.css', // Block editor CSS.
		array( 'wp-edit-blocks' ) // Dependency to include the CSS after it.
		// filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.editor.build.css' ) // Version: File modification time.
	);

	// wp_enqueue_style( 'main-stylesheet', get_stylesheet_directory_uri() . '/dist/assets/css/' . foundationpress_asset_path( 'app.css' ), array(), '2.10.4', 'all' );

}
// Hook: Editor assets.
add_action( 'enqueue_block_editor_assets', 'blocks_cgb_editor_assets' );