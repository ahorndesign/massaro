const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks;
const { registerStore, withSelect } = wp.data;
const {
	InnerBlocks,
	InspectorControls,
	MediaUpload,
} = wp.editor;
const {
	TabPanel,
	Dashicon,
	FormFileUpload,
	TextControl,
	FormTokenField,
	RangeControl,
	SelectControl,
	ServerSideRender,
	Panel, 
	PanelBody, 
	PanelRow
} = wp.components;

var p = new Array(),
	t = new Array(),
	global = window.global;
	// apiFetch( { path: '/wp-json/wp/v2/types' } ).then( posts => {
		// 	p = posts;
		// } );
		
		t = global.appliances;
// Object(external_this_wp_url_["addQueryArgs"])("/wp/v2/categories")
registerBlockType( 'hswp/appliances', {
    title: 'Appliances',
    icon: 'grid-view',
    category: 'widgets',
    attributes: {
		post_type: {
			type: 'string',
			default: 'appliance'
		},
		posts_per_page:{
			type: 'number',
			default: '1'
		}, 
		category:{
			type: 'array',
			default: []
		}, 
	},
    edit: ( props ) => {
		const { posts } = props;

	
		const {
					post_type,
					posts_per_page,
					category,
				} = props.attributes;
    	{/* post type options */}
        var options = [];
        Object.keys(p).map(function(objectKey, index) {
			options.push( 
					{
		                label: p[objectKey].name,
		                value: objectKey
		            },
				)
		});
		var terms = [];
        Object.keys(t).map(function(objectKey, index) {
			terms.push( 
					{
		                label: t[objectKey].name,
						value: t[objectKey].term_id
		            },
				)
		});
		const termsControl = (terms.length > 1) ? (
			<FormTokenField 
				label = "Categories"
				value={ category.map( ( term ) => {
												for(var i=0; i<terms.length; i++) {
													if(terms[i]['value']== term) {
														return terms[i].label;
													}
												}
									} ) } 
				suggestions={ terms.map( ( term ) => term.label ) } 
        		onChange={ ( tokens ) => {
											props.setAttributes( {'category': tokens.map( ( term ) => {
												for(var i=0; i<terms.length; i++) {
													if(terms[i]['label'].indexOf(term)!=-1) {
														return terms[i].value;
													}
												}
											} )} )
						} }  
        		placeholder="Start typing..."
    />) : null;
        const controls = [
			
			<PanelBody
				title={ __( 'General Settings' ) }
				initialOpen={ true }
			>
			
			   {termsControl}
			    <RangeControl
			        label="Posts Per Page"
			        value={ posts_per_page }
			        onChange={ ( posts_per_page ) => props.setAttributes( {'posts_per_page': posts_per_page} ) }
					min={ 1 }
					max={ 10 }
			    />
			</PanelBody>
		];

        return (<div>
        	<InspectorControls>
				{controls}
			</InspectorControls>
			<ServerSideRender
				block="hswp/appliances"
				attributes={ props.attributes }
			/>
        	</div>
        	);

    } ,

    save() {
        // Rendering in PHP
        return null;
    },
} );