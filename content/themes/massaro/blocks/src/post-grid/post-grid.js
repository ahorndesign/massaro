const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks;
const { withSelect } = wp.data;
const {
	InnerBlocks,
	InspectorControls,
	MediaUpload,
} = wp.editor;
const {
	TabPanel,
	Dashicon,
	FormFileUpload,
	TextControl,
	RangeControl,
	SelectControl,
	ServerSideRender,
	Panel, 
	PanelBody, 
	PanelRow
} = wp.components;
import apiFetch from '@wordpress/api-fetch';

var p = new Array(),
	t = new Array(),
	global = window.global;
 apiFetch( { path: '/wp-json/wp/v2/types' } ).then( posts => {
    p = posts;
} );

 apiFetch( { path: '/wp-json/hlwp/v1/terms/1' } ).then( terms => {
    t = terms;
} );

const columnsOptions = [
	{
		label: "1 column",
		value: 1
	},
	{
		label: "2 columns",
		value: 2
	},
	{
		label: "3 columns",
		value: 3
	},
	{
		label: "4 columns",
		value: 4
	},
	{
		label: "5 columns",
		value: 5
	},
	{
		label: "6 columns",
		value: 6
	},
	{
		label: "7 columns",
		value: 7
	},
	{
		label: "8 columns",
		value: 8
	},
	{
		label: "9 columns",
		value: 9
	},
	{
		label: "10 columns",
		value: 10
	},
	{
		label: "11 columns",
		value: 11
	},
	{
		label: "12 columns",
		value: 12
	}
]

// Object(external_this_wp_url_["addQueryArgs"])("/wp/v2/categories")
registerBlockType( 'edx/post-grid', {
    title: 'Post Grid',
    icon: 'grid-view',
    category: 'widgets',
    attributes: {
		post_type: {
			type: 'string',
			default: 'post'
		},
		posts_per_page:{
			type: 'number',
			default: '1'
		}, 
		category:{
			type: 'string',
			default: ''
		}, 
		template:{
			type: 'string',
			default: ''
		}, 
		small:{
			type: 'string',
			default: 1
		},
		medium:{
			type: 'string',
			default: 1
		},
		large:{
			type: 'string',
			default: 1
		}, 
	},
    edit: withSelect( (select, props ) => {
    	const {
    				posts_per_page,
					post_type,
					category,
					template
				} = props.attributes;
        return {
            posts: select( 'core' ).getEntityRecords( 'postType', post_type, { per_page: posts_per_page } )
        };
    } )( ( props ) => {
		const { posts, className, attributes, setAttributes } = props;
		const {
					post_type,
					posts_per_page,
					category,
					template, 
					small, 
					medium,
					large
				} = props.attributes;
       
    	{/* post type options */}
        var options = [];
        Object.keys(p).map(function(objectKey, index) {
			options.push( 
					{
		                label: p[objectKey].name,
		                value: objectKey
		            },
				)
		});

		var terms = [];
        Object.keys(t).map(function(objectKey, index) {
			terms.push( 
					{
		                label: t[objectKey],
		                value: objectKey
		            },
				)
		});

        var templates = [];
		Object.keys(global.templates).map(function(objectKey, index) {
			templates.push( 
					{
		                label: global.templates[objectKey],
		                value: objectKey
		            },
				)
		});


		
        const controls = [
			
			<PanelBody
				title={ __( 'General Settings' ) }
				initialOpen={ true }
			>
				<SelectControl

			        label="Post Type"
			        value={post_type}
			        options={ options }
			       	onChange={ ( post_type ) => {
											props.setAttributes( {'post_type': post_type} );
										} }/>
			    <SelectControl

			        label="Term"
			        value={category}
			        options={ terms }
			        onChange={ ( category ) => {
											props.setAttributes( {'category': category} );
										} }/>
			    <SelectControl

			        label="Templates"
			        value={template}
			        options={ templates }
			        onChange={ ( template ) => {
											props.setAttributes( {'template': template} );
										} }/>
			    <RangeControl
			        label="Posts Per Page"
			        value={ posts_per_page }
			        onChange={ ( posts_per_page ) => props.setAttributes( {'posts_per_page': posts_per_page} ) }
					min={ 1 }
			    />
			</PanelBody>
		];


		const columns = [
			
			<PanelBody
				title={ __( 'Display Options' ) }
				initialOpen={ true }
			>
				<SelectControl

			        label="Small"
			        value={small}
			        options={ columnsOptions }
			       	onChange={ ( columns ) => {
											props.setAttributes( {'small': columns} );
										} }/>
			    <SelectControl

					label="Medium"
					value={medium}
					options={ columnsOptions }
   					onChange={ ( columns ) => {
						props.setAttributes( {'medium': columns} );
					} }/>
			    <SelectControl

					label="Large"
					value={large}
					options={ columnsOptions }
					onChange={ ( columns ) => {
						props.setAttributes( {'large': columns} );
					} }/>
			    <RangeControl
			        label="Posts Per Page"
			        value={ posts_per_page }
			        onChange={ ( posts_per_page ) => props.setAttributes( {'posts_per_page': posts_per_page} ) }
					min={ 1 }
			    />
			</PanelBody>
		];

		 if ( ! posts ) {
            return "Loading...";
        }

        if ( posts && posts.length === 0 ) {
            return (<div> <InspectorControls>

					{controls}
					{columns}
				</InspectorControls> "No posts" </div>);
        }
       
    	var results = posts.map(post=> (
    		<div>
    		<a className={ className } href={ post.link }>
	            { post.title.rendered }
	        </a></div>
    	))
        let post = posts[ 0 ];

        return (<div>
        	<InspectorControls>
					{controls}
					{columns}

				</InspectorControls>
				 <ServerSideRender
	                block="edx/post-grid"
	                attributes={ props.attributes }
	            />
        	</div>
        	);

    } ),

    save() {
        // Rendering in PHP
        return null;
    },
} );