<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<header>
	<?php
		$header_title = get_theme_mod( 'archive_resources_title');
		$header_subtitle = get_theme_mod( 'archive_resources_desc');
		$header_image = get_theme_mod( 'header_resources_image');
	?>
	<section class="archive-hero g-hero resources-header hero">
		<div class="grid-container">
			<div class="g-hero-inner grid-x">
				<div class="cell small-12 medium-6">
					<?php if ( is_search() ) { ?>
						<h1 class="entry-title"><?php _e( 'Search Results for', 'foundationpress' ); ?> "<?php echo get_search_query(); ?>"</h1>
					<?php } else { ?>
						<h1><?php echo $header_title; ?></h1>
						<p><?php echo $header_subtitle; ?></p>
					<?php } ?>
				</div>
				
				<div class="cell small-12 medium-6">
					<img src="<?php echo 	$header_image; ?>" alt="Header Image">
				</div>
			</div>
		</div>
	</section>
</header>

<div class="main-container">
	<div class="main-grid">
		<main class="main-content-full-width">
			<div class="grid-x grid-margin-x resources-nav-container">
				<div class="cell small-12 medium-3 resources-nav-search">
					<?php get_search_form(); ?>
				</div>
				<?php if ( !is_search() ) {?>
					<div class="cell small-12 medium-auto resources-nav">
						<?php echo resources_nav(); ?>
					</div>
					<?php 
					$resources_filter_arg = resources_filter();
					if ( $resources_filter_arg ) { 
						$resources_filter = wp_nav_menu($resources_filter_arg);
					}
					if (!empty($resources_filter)) {
						if ( is_tax() || is_category() || is_tag() ) {
							$queried_object = get_queried_object();
							$filter_button_name = $queried_object->name;
						} else {
							$filter_button_name = __('Filters', 'massaro');
						}
					?>

					<div class="cell small-12 medium-shrink resources-nav-filter">
						<button class="button filter-button" type="button" data-toggle="filter-dropdown"><?php echo $filter_button_name; ?></button>
						<div class="dropdown-pane filter-menus-container large" data-position="bottom" data-alignment="right" id="filter-dropdown" data-dropdown data-auto-focus="true">
							<?php echo $resources_filter; ?>
						</div>
					</div>
				<?php }
				} ?>
			</div>
			
			<?php if ( have_posts() ) : ?>
				<div class="grid-x grid-margin-x small-up-1 medium-up-3">
					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'template-parts/listing', 'resources' ); ?>
					<?php endwhile; ?>
				</div>
			<?php else : ?>
				<?php get_template_part( 'template-parts/content', 'none' ); ?>	
			<?php endif; // End have_posts() check. ?>
			
			<?php /* Display navigation to next/previous pages when applicable */ ?>
			<?php
			if ( function_exists( 'foundationpress_pagination' ) ) :
				foundationpress_pagination();
			elseif ( is_paged() ) :
			?>
				<nav id="post-nav">
					<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
					<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
				</nav>
			<?php endif; ?>

			<div class="keep-in-touch-section-archive grid-container">
				<?php dynamic_sidebar( 'archive-footer-top-header-widgets' ); ?>
				<div class="grid-x grid-margin-x">
					<?php dynamic_sidebar( 'archive-footer-top-widgets' ); ?>
				</div>
			</div>

		</main>
	</div>
</div>
<?php get_footer();